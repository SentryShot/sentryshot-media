# Convert

**State: experimental**

Pure Rust pixel format converter.

Supported conversions:

- [x] yuv420p -> rgb24
- [x] yuv420p -> gray8
- [x] yuvj420p -> rgb24
- [x] yuvj420p -> gray8

<br>

## License

This library is licensed under [MPL-2.0](https://www.mozilla.org/en-US/MPL/2.0)

[What is the Mozilla Public License?](https://www.mozilla.org/en-US/MPL/2.0/FAQ)