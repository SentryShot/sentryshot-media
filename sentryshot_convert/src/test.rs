// SPDX-License-Identifier: MPL-2.0+

#![allow(clippy::unwrap_used)]

use crate::PixelFormatConverter;
use pretty_assertions::assert_eq;
use pretty_hex::pretty_hex;
use sentryshot_util::{pixfmt::PixelFormat, ColorRange, Frame};
use std::num::NonZeroU16;
use test_case::test_case;

const YUV420P_FRAME: &[u8] = include_bytes!("../../testdata/yuv420p_64x64.bin");
const GRAY8_FRAME: &[u8] = include_bytes!("../../testdata/gray8_64x64_from_yuv420p.bin");

#[test_case(
    64, 64, ColorRange::MPEG,
    PixelFormat::YUV420P, PixelFormat::RGB24,
    YUV420P_FRAME,
    include_bytes!("../../testdata/rgb24_64x64_from_yuv420p.bin");
    "yuv420 to rgb24"
)]
#[test_case(
    64, 64, ColorRange::JPEG,
    PixelFormat::YUVJ420P, PixelFormat::RGB24,
    YUV420P_FRAME,
    include_bytes!("../../testdata/rgb24_64x64_from_yuvj420p.bin");
    "yuvj420 to rgb24"
)]
#[test_case(
    4, 4, ColorRange::UNSPECIFIED,
    PixelFormat::YUV420P, PixelFormat::RGB24,
    &[0; 24],
    &[0, 0x89, 0, 0, 0x89, 0, 0, 0x89, 0, 0, 0x89, 0,
      0, 0x89, 0, 0, 0x89, 0, 0, 0x89, 0, 0, 0x89, 0,
      0, 0x89, 0, 0, 0x89, 0, 0, 0x89, 0, 0, 0x89, 0,
      0, 0x89, 0, 0, 0x89, 0, 0, 0x89, 0, 0, 0x89, 0];
    "yuv420 to rgb24 out of bounds"
)]
#[test_case(
    64, 64, ColorRange::MPEG,
    PixelFormat::YUV420P, PixelFormat::GRAY8,
    YUV420P_FRAME,
    GRAY8_FRAME;
    "yuv420 to gray8"
)]
#[test_case(
    64, 64, ColorRange::UNSPECIFIED,
    PixelFormat::YUVJ420P, PixelFormat::GRAY8,
    YUV420P_FRAME,
    GRAY8_FRAME;
    "yuvj420 to gray8"
)]
fn test_converter(
    width: u16,
    heigth: u16,
    color_range: ColorRange,
    src_format: PixelFormat,
    dst_format: PixelFormat,
    input: &[u8],
    want: &[u8],
) {
    let width = NonZeroU16::new(width).unwrap();
    let heigth = NonZeroU16::new(heigth).unwrap();

    let mut src_frame = Frame::from_raw(input, src_format, width, heigth, 1).unwrap();
    src_frame.set_color_range(color_range);

    let mut converter =
        PixelFormatConverter::new(width, heigth, color_range, src_format, dst_format).unwrap();

    let mut dst_frame = Frame::new();
    converter.convert(&src_frame, &mut dst_frame).unwrap();

    let mut raw = Vec::new();
    dst_frame.copy_to_buffer(&mut raw, 1).unwrap();

    assert_eq!(want.len(), raw.len());
    assert_eq!(pretty_hex(&want), pretty_hex(&raw));
}

/*
#[test]
fn test_yuv420p_to_rgb24_fuzz() {
    for w in 1..3000 {
        println!("{w}");
        for h in (2..3000).step_by(2) {
            let width = w;
            let height = h;
            let src_fmt = PixelFormat::YUV420P;
            let dst_fmt = PixelFormat::RGB24;

            let buf = vec![0; usize::from(width) * usize::from(height) * 2];
            let src_frame = Frame::from_raw(&buf, src_fmt, width, height, 1).unwrap();

            let mut converter = PixelFormatConverter::new(
                width,
                height,
                ColorRange::UNSPECIFIED,
                src_fmt,
                dst_fmt,
            )
            .unwrap();

            let mut dst_frame = Frame::new();
            converter.convert(&src_frame, &mut dst_frame).unwrap();
        }
    }
}
*/
