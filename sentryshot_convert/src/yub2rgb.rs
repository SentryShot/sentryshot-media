// SPDX-License-Identifier: MPL-2.0+

use crate::{
    ConvertFunc, PixelFormatConverter, YuvTables, YUVRGB_TABLE_HEADROOM,
    YUVRGB_TABLE_HEADROOM_USIZE, YUVRGB_TABLE_LUMA_HEADROOM, YUVRGB_TABLE_SIZE, YUV_TABLE_SIZE,
};
use sentryshot_util::{clip_u8, pixfmt::PixelFormat, ColorRange};
use std::{array, num::TryFromIntError};
use thiserror::Error;

// Color space conversion coefficients for YCbCr -> RGB mapping.
//
// Entries are {crv, cbu, cgu, cgv}
//   crv = (255 / 224) * 65536 * (1 - cr) / 0.5
//   cbu = (255 / 224) * 65536 * (1 - cb) / 0.5
//   cgu = (255 / 224) * 65536 * (cb / cg) * (1 - cb) / 0.5
//   cgv = (255 / 224) * 65536 * (cr / cg) * (1 - cr) / 0.5
//
// where Y = cr * R + cg * G + cb * B and cr + cg + cb = 1.
pub(crate) const FF_YUV2RGB_COEFFS: [[i32; 4]; 11] = [
    [117_489, 138_438, 13975, 34925], // no sequence_display_extension.
    [117_489, 138_438, 13975, 34925], // ITU-R Rec. 709 (1990).
    [104_597, 132_201, 25675, 53279], // unspecified.
    [104_597, 132_201, 25675, 53279], // reserved.
    [104_448, 132_798, 24759, 53109], // FCC.
    [104_597, 132_201, 25675, 53279], // ITU-R Rec. 624-4 System B, G.
    [104_597, 132_201, 25675, 53279], // SMPTE 170M.
    [117_579, 136_230, 16907, 35559], // SMPTE 240M (1987).
    [0, 0, 0, 0],                     // YCgCo.
    [110_013, 140_363, 12277, 42626], // Bt-2020-NCL.
    [110_013, 140_363, 12277, 42626], // Bt-2020-CL.
];

#[allow(
    clippy::erasing_op,
    clippy::identity_op,
    clippy::too_many_lines,
    clippy::many_single_char_names,
    clippy::unwrap_used
)]
pub(crate) fn yuv2rgb_24(
    c: &PixelFormatConverter,
    src: &[Vec<u8>],
    src_stride: &mut [usize],
    src_slice_h: i32,
    dst: &mut [Vec<u8>],
    dst_stride: &[usize],
) {
    let tables = c.yuv_tables.as_ref().expect("tables to be set");

    if c.src_format == PixelFormat::YUV422P {
        src_stride[1] *= 2;
        src_stride[2] *= 2;
    }

    let calc_g = |u: usize, v: usize| -> &[u8] {
        let offset1 = isize::try_from(tables.gu[u + YUVRGB_TABLE_HEADROOM_USIZE]).unwrap();
        let offset2 = isize::from(tables.gv[v + YUVRGB_TABLE_HEADROOM_USIZE]);
        &tables.yuv_table[usize::try_from(offset1 + offset2).unwrap()..]
    };

    for y in (0..src_slice_h).step_by(2) {
        let yd = y;

        let mut dst_1 = &mut dst[0][usize::try_from(yd).unwrap() * dst_stride[0]..];
        let off = (usize::try_from(yd + 1).unwrap() * dst_stride[0])
            - (usize::try_from(yd).unwrap() * dst_stride[0]);
        let mut r: &[u8];
        let mut g: &[u8];
        let mut b: &[u8];
        let mut py_1 = &src[0][usize::try_from(y).unwrap() * src_stride[0]..];
        let mut py_2 = &py_1[src_stride[0]..];
        let mut pu = &src[1][usize::try_from(y >> 1).unwrap() * src_stride[1]..];
        let mut pv = &src[2][usize::try_from(y >> 1).unwrap() * src_stride[2]..];
        let h_size = c.width.get() >> 3;

        for _ in (0..h_size).rev() {
            // LOADCHROMA.
            let mut u = usize::from(pu[0]);
            let mut v = usize::from(pv[0]);
            r = &tables.yuv_table[tables.rv[v + YUVRGB_TABLE_HEADROOM_USIZE]..];
            g = calc_g(u, v);
            b = &tables.yuv_table[tables.bu[u + YUVRGB_TABLE_HEADROOM_USIZE]..];

            // PUTRGB24.
            let mut y = usize::from(py_1[2 * 0]);
            dst_1[6 * 0 + 0] = r[y];
            dst_1[6 * 0 + 1] = g[y];
            dst_1[6 * 0 + 2] = b[y];
            y = usize::from(py_1[2 * 0 + 1]);
            dst_1[6 * 0 + 3] = r[y];
            dst_1[6 * 0 + 4] = g[y];
            dst_1[6 * 0 + 5] = b[y];

            // PUTRGB24.
            y = usize::from(py_2[2 * 0]);
            dst_1[6 * 0 + 0 + off] = r[y];
            dst_1[6 * 0 + 1 + off] = g[y];
            dst_1[6 * 0 + 2 + off] = b[y];
            y = usize::from(py_2[2 * 0 + 1]);
            dst_1[6 * 0 + 3 + off] = r[y];
            dst_1[6 * 0 + 4 + off] = g[y];
            dst_1[6 * 0 + 5 + off] = b[y];

            // LOADCHROMA.
            u = usize::from(pu[1]);
            v = usize::from(pv[1]);
            r = &tables.yuv_table[tables.rv[v + YUVRGB_TABLE_HEADROOM_USIZE]..];
            g = calc_g(u, v);
            b = &tables.yuv_table[tables.bu[u + YUVRGB_TABLE_HEADROOM_USIZE]..];

            // PUTRGB24.
            y = usize::from(py_2[2 * 1]);
            dst_1[6 * 1 + 0 + off] = r[y];
            dst_1[6 * 1 + 1 + off] = g[y];
            dst_1[6 * 1 + 2 + off] = b[y];
            y = usize::from(py_2[2 * 1 + 1]);
            dst_1[6 * 1 + 3 + off] = r[y];
            dst_1[6 * 1 + 4 + off] = g[y];
            dst_1[6 * 1 + 5 + off] = b[y];

            // PUTRGB24.
            y = usize::from(py_1[2 * 1]);
            dst_1[6 * 1 + 0] = r[y];
            dst_1[6 * 1 + 1] = g[y];
            dst_1[6 * 1 + 2] = b[y];
            y = usize::from(py_1[2 * 1 + 1]);
            dst_1[6 * 1 + 3] = r[y];
            dst_1[6 * 1 + 4] = g[y];
            dst_1[6 * 1 + 5] = b[y];

            // LOADCHROMA.
            u = usize::from(pu[2]);
            v = usize::from(pv[2]);
            r = &tables.yuv_table[tables.rv[v + YUVRGB_TABLE_HEADROOM_USIZE]..];
            g = calc_g(u, v);
            b = &tables.yuv_table[tables.bu[u + YUVRGB_TABLE_HEADROOM_USIZE]..];

            // PUTRGB24.
            y = usize::from(py_1[2 * 2]);
            dst_1[6 * 2 + 0] = r[y];
            dst_1[6 * 2 + 1] = g[y];
            dst_1[6 * 2 + 2] = b[y];
            y = usize::from(py_1[2 * 2 + 1]);
            dst_1[6 * 2 + 3] = r[y];
            dst_1[6 * 2 + 4] = g[y];
            dst_1[6 * 2 + 5] = b[y];

            // PUTRGB24.
            y = usize::from(py_2[2 * 2]);
            dst_1[6 * 2 + 0 + off] = r[y];
            dst_1[6 * 2 + 1 + off] = g[y];
            dst_1[6 * 2 + 2 + off] = b[y];
            y = usize::from(py_2[2 * 2 + 1]);
            dst_1[6 * 2 + 3 + off] = r[y];
            dst_1[6 * 2 + 4 + off] = g[y];
            dst_1[6 * 2 + 5 + off] = b[y];

            // LOADCHROMA.
            u = usize::from(pu[3]);
            v = usize::from(pv[3]);
            r = &tables.yuv_table[tables.rv[v + YUVRGB_TABLE_HEADROOM_USIZE]..];
            g = calc_g(u, v);
            b = &tables.yuv_table[tables.bu[u + YUVRGB_TABLE_HEADROOM_USIZE]..];

            // PUTRGB24.
            y = usize::from(py_2[2 * 3]);
            dst_1[6 * 3 + 0 + off] = r[y];
            dst_1[6 * 3 + 1 + off] = g[y];
            dst_1[6 * 3 + 2 + off] = b[y];
            y = usize::from(py_2[2 * 3 + 1]);
            dst_1[6 * 3 + 3 + off] = r[y];
            dst_1[6 * 3 + 4 + off] = g[y];
            dst_1[6 * 3 + 5 + off] = b[y];

            // PUTRGB24.
            y = usize::from(py_1[2 * 3]);
            dst_1[6 * 3 + 0] = r[y];
            dst_1[6 * 3 + 1] = g[y];
            dst_1[6 * 3 + 2] = b[y];
            y = usize::from(py_1[2 * 3 + 1]);
            dst_1[6 * 3 + 3] = r[y];
            dst_1[6 * 3 + 4] = g[y];
            dst_1[6 * 3 + 5] = b[y];

            // ENDYUV2RGBLINE.
            pu = &pu[4..];
            pv = &pv[4..];
            py_1 = &py_1[8..];
            py_2 = &py_2[8..];
            dst_1 = &mut dst_1[24..];
        }
        if c.width.get() & (4 >> 0) != 0 {
            // LOADCHROMA.
            let mut u = usize::from(pu[0]);
            let mut v = usize::from(pv[0]);
            r = &tables.yuv_table[tables.rv[v + YUVRGB_TABLE_HEADROOM_USIZE]..];
            g = calc_g(u, v);
            b = &tables.yuv_table[tables.bu[u + YUVRGB_TABLE_HEADROOM_USIZE]..];

            // PUTRGB24.
            let mut y = usize::from(py_1[2 * 0]);
            dst_1[6 * 0 + 0] = r[y];
            dst_1[6 * 0 + 1] = g[y];
            dst_1[6 * 0 + 2] = b[y];
            y = usize::from(py_1[2 * 0 + 1]);
            dst_1[6 * 0 + 3] = r[y];
            dst_1[6 * 0 + 4] = g[y];
            dst_1[6 * 0 + 5] = b[y];

            // PUTRGB24.
            y = usize::from(py_2[2 * 0]);
            dst_1[6 * 0 + 0 + off] = r[y];
            dst_1[6 * 0 + 1 + off] = g[y];
            dst_1[6 * 0 + 2 + off] = b[y];
            y = usize::from(py_2[2 * 0 + 1]);
            dst_1[6 * 0 + 3 + off] = r[y];
            dst_1[6 * 0 + 4 + off] = g[y];
            dst_1[6 * 0 + 5 + off] = b[y];

            // LOADCHROMA.
            u = usize::from(pu[1]);
            v = usize::from(pv[1]);
            r = &tables.yuv_table[tables.rv[v + YUVRGB_TABLE_HEADROOM_USIZE]..];
            g = calc_g(u, v);
            b = &tables.yuv_table[tables.bu[u + YUVRGB_TABLE_HEADROOM_USIZE]..];

            // PUTRGB24.
            y = usize::from(py_2[2 * 1]);
            dst_1[6 * 1 + 0 + off] = r[y];
            dst_1[6 * 1 + 1 + off] = g[y];
            dst_1[6 * 1 + 2 + off] = b[y];
            y = usize::from(py_2[2 * 1 + 1]);
            dst_1[6 * 1 + 3 + off] = r[y];
            dst_1[6 * 1 + 4 + off] = g[y];
            dst_1[6 * 1 + 5 + off] = b[y];

            // PUTRGB24.
            y = usize::from(py_1[2 * 1]);
            dst_1[6 * 1 + 0] = r[y];
            dst_1[6 * 1 + 1] = g[y];
            dst_1[6 * 1 + 2] = b[y];
            y = usize::from(py_1[2 * 1 + 1]);
            dst_1[6 * 1 + 3] = r[y];
            dst_1[6 * 1 + 4] = g[y];
            dst_1[6 * 1 + 5] = b[y];

            pu = &pu[(4 >> 1)..];
            pv = &pv[(4 >> 1)..];
            if pv.len() > (8 >> 1) {
                py_1 = &pv[(8 >> 1)..];
                py_2 = &pv[(8 >> 1)..];
            }
            dst_1 = &mut dst_1[(24 >> 1)..];
        }
        if c.width.get() & (4 >> 1) != 0 {
            // LOADCHROMA.
            let u = usize::from(pu[0]);
            let v = usize::from(pv[0]);
            r = &tables.yuv_table[tables.rv[v + YUVRGB_TABLE_HEADROOM_USIZE]..];
            g = calc_g(u, v);
            b = &tables.yuv_table[tables.bu[u + YUVRGB_TABLE_HEADROOM_USIZE]..];

            // PUTRGB24.
            let mut y = usize::from(py_1[2 * 0]);
            dst_1[6 * 0 + 0] = r[y];
            dst_1[6 * 0 + 1] = g[y];
            dst_1[6 * 0 + 2] = b[y];
            y = usize::from(py_1[2 * 0 + 1]);
            dst_1[6 * 0 + 3] = r[y];
            dst_1[6 * 0 + 4] = g[y];
            dst_1[6 * 0 + 5] = b[y];

            // PUTRGB24.
            y = usize::from(py_2[2 * 0]);
            dst_1[6 * 0 + 0 + off] = r[y];
            dst_1[6 * 0 + 1 + off] = g[y];
            dst_1[6 * 0 + 2 + off] = b[y];
            y = usize::from(py_2[2 * 0 + 1]);
            dst_1[6 * 0 + 3 + off] = r[y];
            dst_1[6 * 0 + 4 + off] = g[y];
            dst_1[6 * 0 + 5 + off] = b[y];
        }
    }
}

pub fn yuv2rgb_get_func(dst_format: PixelFormat) -> Option<ConvertFunc> {
    match dst_format {
        PixelFormat::RGB24 => Some(yuv2rgb_24),
        _ => None,
    }
}

#[derive(Debug, Error)]
pub enum FillTableError {
    #[error("try from: {0}")]
    TryFrom(TryFromIntError, String),
}

fn fill_table(
    elemsize: i64,
    inc: i64,
    yoffs: u16,
) -> Result<[usize; YUVRGB_TABLE_SIZE], FillTableError> {
    use FillTableError::TryFrom;
    let mut table: [usize; YUVRGB_TABLE_SIZE] = array::from_fn(|i| i);

    let off = i64::from(yoffs) - elemsize * (inc >> 9);

    for i in 0..256 + 2 * YUVRGB_TABLE_HEADROOM {
        let cb: i64 = i64::from(clip_u8(i32::from(i) - i32::from(YUVRGB_TABLE_HEADROOM))) * inc;
        table[usize::from(i)] = usize::try_from(off + elemsize * (cb >> 16))
            .map_err(|e| TryFrom(e, "table usize".to_owned()))?;
    }
    Ok(table)
}

fn fill_gv_table(elemsize: i64, inc: i64) -> Result<[i16; YUVRGB_TABLE_SIZE], FillTableError> {
    use FillTableError::TryFrom;
    let mut table = [0; YUVRGB_TABLE_SIZE];
    let off = -(inc >> 9);

    for i in 0..256 + 2 * YUVRGB_TABLE_HEADROOM {
        let cb: i64 = i64::from(clip_u8(i32::from(i) - i32::from(YUVRGB_TABLE_HEADROOM))) * inc;
        table[usize::from(i)] = i16::try_from(elemsize * (off + (cb >> 16)))
            .map_err(|e| TryFrom(e, "table usize".to_owned()))?;
    }
    Ok(table)
}

#[derive(Debug, Error)]
pub enum Yuv2RgbInitTablesError {
    #[error("fill table: {0}")]
    FillTable(#[from] FillTableError),

    #[error("unsupported bpp: {0}")]
    UnsupportedBpp(u8),
}

#[allow(clippy::similar_names)]
pub(crate) fn yuv2rgb_init_tables(
    bpp: u8,
    inv_table: &[i32; 4],
    color_range: ColorRange,
    brightness: i32,
    contrast: i32,
    saturation: i32,
) -> Result<YuvTables, Yuv2RgbInitTablesError> {
    //const int bpp = c->dstFormatBpp;
    //int i, av_uninit(abase);
    let yoffs = if color_range == ColorRange::MPEG {
        326 + YUVRGB_TABLE_LUMA_HEADROOM
    } else {
        384 + YUVRGB_TABLE_LUMA_HEADROOM
    };
    //const int yoffs = (fullRange ? 384 : 326) + YUVRGB_TABLE_LUMA_HEADROOM;

    let mut crv: i64 = inv_table[0].into();
    let mut cbu: i64 = inv_table[1].into();

    let mut cgu: i64 = -i64::from(inv_table[2]);
    let mut cgv: i64 = -i64::from(inv_table[3]);
    let mut cy: i64 = 1 << 16;
    let mut oy: i64 = 0;
    //int64_t yb = 0;

    if color_range == ColorRange::MPEG {
        cy = (cy * 255) / 219;
        oy = 16 << 16;
    } else {
        crv = (crv * 224) / 255;
        cbu = (cbu * 224) / 255;
        cgu = (cgu * 224) / 255;
        cgv = (cgv * 224) / 255;
    }

    let contrast = i64::from(contrast);
    let saturation = i64::from(saturation);
    cy = (cy * contrast) >> 16;
    crv = (crv * contrast * saturation) >> 32;
    cbu = (cbu * contrast * saturation) >> 32;
    cgu = (cgu * contrast * saturation) >> 32;
    cgv = (cgv * contrast * saturation) >> 32;
    oy -= 256 * i64::from(brightness);

    /*c->uOffset = 0x0400040004000400LL;
    c->vOffset = 0x0400040004000400LL;
    c->yCoeff = roundToInt16(cy * (1 << 13)) * 0x0001000100010001ULL;
    c->vrCoeff = roundToInt16(crv * (1 << 13)) * 0x0001000100010001ULL;
    c->ubCoeff = roundToInt16(cbu * (1 << 13)) * 0x0001000100010001ULL;
    c->vgCoeff = roundToInt16(cgv * (1 << 13)) * 0x0001000100010001ULL;
    c->ugCoeff = roundToInt16(cgu * (1 << 13)) * 0x0001000100010001ULL;
    c->yOffset = roundToInt16(oy * (1 << 3)) * 0x0001000100010001ULL;

    c->yuv2rgb_y_coeff = (int16_t)roundToInt16(cy * (1 << 13));
    c->yuv2rgb_y_offset = (int16_t)roundToInt16(oy * (1 << 9));
    c->yuv2rgb_v2r_coeff = (int16_t)roundToInt16(crv * (1 << 13));
    c->yuv2rgb_v2g_coeff = (int16_t)roundToInt16(cgv * (1 << 13));
    c->yuv2rgb_u2g_coeff = (int16_t)roundToInt16(cgu * (1 << 13));
    c->yuv2rgb_u2b_coeff = (int16_t)roundToInt16(cbu * (1 << 13));*/

    // scale coefficients by cy
    crv = ((crv * (1 << 16)) + 0x8000) / i64::max(cy, 1);
    cbu = ((cbu * (1 << 16)) + 0x8000) / i64::max(cy, 1);
    cgu = ((cgu * (1 << 16)) + 0x8000) / i64::max(cy, 1);
    cgv = ((cgv * (1 << 16)) + 0x8000) / i64::max(cy, 1);

    match bpp {
        24 | 48 => {
            let mut yb = -(384 << 16) - i64::from(YUVRGB_TABLE_LUMA_HEADROOM) * cy - oy;

            let mut yuv_table: [u8; YUV_TABLE_SIZE] = array::from_fn(|_| 0);
            #[allow(clippy::as_conversions, clippy::cast_possible_truncation)]
            for entry in &mut yuv_table {
                *entry = clip_u8((yb as i32 + 0x8000) >> 16);
                yb += cy;
            }

            Ok(YuvTables {
                yuv_table,
                gv: fill_gv_table(1, cgv)?,
                rv: fill_table(1, crv, yoffs)?,
                gu: fill_table(1, cgu, yoffs)?,
                bu: fill_table(1, cbu, yoffs)?,
            })
        }
        _ => Err(Yuv2RgbInitTablesError::UnsupportedBpp(bpp)),
    }
}
