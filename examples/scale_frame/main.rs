use sentryshot_scale::{Frame, PixelFormat, Scaler};
use std::num::NonZeroU16;

const YUV420P_FRAME: &[u8] = include_bytes!("../../testdata/yuv420p_64x64.bin");

fn main() {
    let src_width = NonZeroU16::new(64).unwrap();
    let src_height = NonZeroU16::new(64).unwrap();
    let pix_fmt = PixelFormat::YUV420P;
    let dst_width = NonZeroU16::new(100).unwrap();
    let dst_height = NonZeroU16::new(40).unwrap();

    // Initialize source frame from rawvideo.
    let src_frame = Frame::from_raw(YUV420P_FRAME, pix_fmt, src_width, src_height, 1).unwrap();

    // Initialize the scaler.
    let mut scaler = Scaler::new(src_width, src_height, pix_fmt, dst_width, dst_height).unwrap();

    // Initialize destination frame.
    let mut dst_frame = Frame::new();

    // Scale the frame.
    scaler.scale(&src_frame, &mut dst_frame).unwrap();

    // Convert the frame to rawvideo.
    let mut buf = Vec::new();
    dst_frame.copy_to_buffer(&mut buf, 1).unwrap();

    // Write the frame to disk.
    let filename = "raw_frame.bin";
    std::fs::write(filename, &buf).unwrap();

    println!("Scaling succeeded. Play the output file with the command:");
    print!(
        "  ffplay -v trace -f rawvideo -pix_fmt {} -s {}x{} {}\n\n",
        dst_frame.pix_fmt(),
        dst_frame.width(),
        dst_frame.height(),
        filename
    )
}
