use sentryshot_ffmpeg_h264::{Frame, H264DecoderBuilder, Packet, PaddedBytes};

// `ffmpeg -i input -vframes 1 -c:v libx264 -bsf:v h264_mp4toannexb output.h264`
const ANNEXB_FRAME: &[u8] = include_bytes!("../../testdata/annexb_frame.bin");

fn main() {
    // Initialize the decoder.
    let mut decoder = H264DecoderBuilder::new().annex_b().unwrap();

    // Send frame.
    decoder
        .send_packet(&Packet::new(&PaddedBytes::new(ANNEXB_FRAME.to_owned())))
        .unwrap();

    // Enter draining mode.
    let mut decoder = decoder.drain().unwrap();

    // Allocate frame.
    let mut frame = Frame::new();

    // Receive the decoded frame.
    decoder.receive_frame(&mut frame).unwrap();

    print!("\nFrame: {}\n\n", frame);

    // Convert the frame to rawvideo.
    let mut buf = Vec::new();
    frame.copy_to_buffer(&mut buf, 1).unwrap();

    // Write the frame to disk.
    let filename = "raw_frame.bin";
    std::fs::write(filename, &buf).unwrap();

    println!("Decoding succeeded. Play the output file with the command:");
    print!(
        "  ffplay -v trace -f rawvideo -pix_fmt {} -s {}x{} {}\n\n",
        frame.pix_fmt(),
        frame.width(),
        frame.height(),
        filename
    )
}
