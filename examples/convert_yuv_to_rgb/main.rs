use sentryshot_convert::{ColorRange, Frame, PixelFormat, PixelFormatConverter};
use std::num::NonZeroU16;

const YUV420P_FRAME: &[u8] = include_bytes!("../../testdata/yuv420p_64x64.bin");

fn main() {
    let width = NonZeroU16::new(64).unwrap();
    let height = NonZeroU16::new(64).unwrap();
    let src_pix_fmt = PixelFormat::YUV420P;
    let dst_pix_fmt = PixelFormat::RGB24;

    // Initialize source frame from rawvideo.
    let src_frame = Frame::from_raw(YUV420P_FRAME, src_pix_fmt, width, height, 1).unwrap();

    // Initialize the converter.
    let mut converter =
        PixelFormatConverter::new(width, height, ColorRange::JPEG, src_pix_fmt, dst_pix_fmt)
            .unwrap();

    // Initialize destination frame.
    let mut dst_frame = Frame::new();

    // Convert the yuv420p frame to rgb24.
    converter.convert(&src_frame, &mut dst_frame).unwrap();

    // Convert the frame to rawvideo.
    let mut buf = Vec::new();
    dst_frame.copy_to_buffer(&mut buf, 1).unwrap();

    // Write the frame to disk.
    let filename = "raw_frame.bin";
    std::fs::write(filename, &buf).unwrap();

    println!("Conversion succeeded. Play the output file with the command:");
    print!(
        "  ffplay -v trace -f rawvideo -pix_fmt {} -s {}x{} {}\n\n",
        dst_frame.pix_fmt(),
        dst_frame.width(),
        dst_frame.height(),
        filename
    )
}
