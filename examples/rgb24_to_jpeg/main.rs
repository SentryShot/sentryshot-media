const RGB24_FRAME: &[u8] = include_bytes!("../../testdata/rgb24_64x64_from_yuv420p.bin");

fn main() {
    let width = 64;
    let height = 64;

    let mut buf = Vec::new();
    let encoder = jpeg_encoder::Encoder::new(&mut buf, 75);

    encoder
        .encode(RGB24_FRAME, width, height, jpeg_encoder::ColorType::Rgb)
        .unwrap();

    let filename = "output.jpeg";
    std::fs::write(filename, &buf).unwrap();

    println!("File written to {}", filename);
}
