use sentryshot_ffmpeg_h264::{Frame, H264DecoderBuilder, Packet, PaddedBytes};

const AVCC_FRAME: &[u8] = include_bytes!("../../testdata/avcc_frame.bin");
const EXTRADATA: &[u8] = include_bytes!("../../testdata/extradata.bin");

fn main() {
    // Initialize the decoder with extradata.
    let mut decoder = H264DecoderBuilder::new()
        .avcc(PaddedBytes::new(EXTRADATA.to_owned()))
        .unwrap();

    // Send frame.
    decoder
        .send_packet(&Packet::new(&PaddedBytes::new(AVCC_FRAME.to_owned())))
        .unwrap();

    // Enter draining mode.
    let mut decoder = decoder.drain().unwrap();

    // Allocate frame.
    let mut frame = Frame::new();

    // Receive the decoded frame.
    decoder.receive_frame(&mut frame).unwrap();

    print!("\nFrame: {}\n\n", frame);

    // Convert the frame to rawvideo.
    let mut buf = Vec::new();
    frame.copy_to_buffer(&mut buf, 1).unwrap();

    // Write the frame to disk.
    let filename = "raw_frame.bin";
    std::fs::write(filename, &buf).unwrap();

    println!("Decoding succeeded. Play the output file with the command:");
    print!(
        "  ffplay -v trace -f rawvideo -pix_fmt {} -s {}x{} {}\n\n",
        frame.pix_fmt(),
        frame.width(),
        frame.height(),
        filename
    )
}
