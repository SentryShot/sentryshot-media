#!/bin/sh

set -e

printf "shellcheck\n"
git ls-files | grep \.sh$ | xargs shellcheck

printf "check fmt\n"
cargo fmt --check

printf "clippy\n"
cargo clippy --no-deps --all-targets -- -D warnings

printf "test\n"
cargo test


if [ "$CI_PIPELINE_EVENT" = "push" ]; then
	cargo login "$CARGO_TOKEN" 

	printf "\npublish padded_bytes\n"
	cargo publish -p sentryshot_padded_bytes || true

	printf "\npublish util\n"
	cargo publish -p sentryshot_util || true

	printf "\npublish convert\n"
	cargo publish -p sentryshot_convert || true

	printf "\npublish ffmpeg_h264_sys\n"
	cargo publish -p sentryshot_ffmpeg_h264_sys || true

	printf "\npublish ffmpeg_h264\n"
	cargo publish -p sentryshot_ffmpeg_h264 || true

	printf "\npublish filter\n"
	cargo publish -p sentryshot_filter || true

	printf "\npublish scale\n"
	cargo publish -p sentryshot_scale || true
fi
