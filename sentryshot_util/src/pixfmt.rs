// SPDX-License-Identifier: MPL-2.0+

use std::{convert::TryFrom, num::NonZeroU16};
use thiserror::Error;

// RGB32 is handled in an endian-specific manner. An RGBA
// color is put together as:
//  (A << 24) | (R << 16) | (G << 8) | B
// This is stored as BGRA on little-endian CPU architectures and ARGB on
// big-endian CPUs.
//
// If the resolution is not a multiple of the chroma subsampling factor
// then the chroma plane resolution must be rounded up.
//
// When the pixel format is palettized RGB32 (AV_PIX_FMT_PAL8), the palettized
// image data is stored in AVFrame.data[0]. The palette is transported in
// AVFrame.data[1], is 1024 bytes long (256 4-byte entries) and is
// formatted the same as in AV_PIX_FMT_RGB32 described above (i.e., it is
// also endian-specific). Note also that the individual RGB32 palette
// components stored in AVFrame.data[1] should be in the range 0..255.
// This is important as many custom PAL8 video codecs that were designed
// to run on the IBM VGA graphics adapter use 6-bit palette components.
//
// For all the 8 bits per pixel formats, an RGB32 palette is in data[1] like
// for pal8. This palette is filled in automatically by the function
// allocating the picture.
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum PixelFormat {
    YUV420P, // 0 planar YUV 4:2:0, 12bpp, (1 Cr & Cb sample per 2x2 Y samples).
    //YUV422, // 1 packed YUV 4:2:2, 16bpp, Y0 Cb Y1 Cr.
    RGB24, // 2 packed RGB 8:8:8, 24bpp, RGBRGB...
    //BGR24, // 3 packed RGB 8:8:8, 24bpp, BGRBGR...
    YUV422P, // 4 planar YUV 4:2:2, 16bpp, (1 Cr & Cb sample per 2x1 Y samples).
    YUV444P, // 5 planar YUV 4:2:2, 16bpp, (1 Cr & Cb sample per 2x1 Y samples).
    //YUV410p // 6 planar YUV 4:1:0,  9bpp, (1 Cr & Cb sample per 4x4 Y samples).
    //YUV411P // 7 planar YUV 4:1:1, 12bpp, (1 Cr & Cb sample per 4x1 Y samples).
    GRAY8, //  8        Y        ,  8bpp.
    //MONOWHITE //  9        Y        ,  1bpp, 0 is white, 1 is black, in each byte pixels are ordered from the msb to the lsb.
    //MONOBLACK // 10        Y        ,  1bpp, 0 is black, 1 is white, in each byte pixels are ordered from the msb to the lsb.
    //PAL8 // 11 8 bits with AV_PIX_FMT_RGB32 palette.
    YUVJ420P, // 12 planar YUV 4:2:0, 12bpp, full scale (JPEG).
    YUVJ422P, // 13 planar YUV 4:2:2, 16bpp, full scale (JPEG).
    YUVJ444P, // 14 planar YUV 4:4:4, 24bpp, full scale (JPEG).
    //UYVY422, // 15 packed YUV 4:2:2, 16bpp, Cb Y0 Cr Y1
    //UYYVYY411, // 16 packed YUV 4:1:1, 12bpp, Cb Y0 Y1 Cr Y2 Y3
    //BGR8, // 17 packed RGB 3:3:2,  8bpp, (msb)2B 3G 3R(lsb)
    //BGR4, // 18 packed RGB 1:2:1 bitstream,  4bpp, (msb)1B 2G 1R(lsb), a byte contains two pixels, the first pixel in the byte is the one composed by the 4 msb bits.
    //BGR4_BYTE, // 19 packed RGB 1:2:1,  8bpp, (msb)1B 2G 1R(lsb).
    //RGB8, // 20 packed RGB 3:3:2,  8bpp, (msb)2R 3G 3B(lsb).
    //RGB4,  // 21 packed RGB 1:2:1 bitstream,  4bpp, (msb)1R 2G 1B(lsb), a byte contains two pixels, the first pixel in the byte is the one composed by the 4 msb bits.
    // RGB4_BYTE, // 22 packed RGB 1:2:1,  8bpp, (msb)1R 2G 1B(lsb).
    //NV12, // 23 planar YUV 4:2:0, 12bpp, 1 plane for Y and 1 plane for the UV components, which are interleaved (first byte U and the following byte V).
    //NV21, // 24 as above, but U and V bytes are swapped.

    //ARGB,      // 25 packed ARGB 8:8:8:8, 32bpp, ARGBARGB...
    //RGBA,      // 26 packed RGBA 8:8:8:8, 32bpp, RGBARGBA...
    //ABGR,      // 27 packed ABGR 8:8:8:8, 32bpp, ABGRABGR...
    //BGRA,      // 28 packed BGRA 8:8:8:8, 32bpp, BGRABGRA...
    //RAY16BE,  // 29        Y        , 16bpp, big-endian.
    //GRAY16LE, // 30        Y        , 16bpp, little-endian.
    //YUV440P,  // 31 planar YUV 4:4:0 (1 Cr & Cb sample per 1x2 Y samples).
    //YUVJ440P,  // 32 planar YUV 4:4:0 full scale (JPEG), deprecated in favor of AV_PIX_FMT_YUV440P and setting color_range.
    YUVA420P, // 33 planar YUV 4:2:0, 20bpp, (1 Cr & Cb sample per 2x2 Y & A samples).
    //RGB48BE,   // 34 packed RGB 16:16:16, 48bpp, 16R, 16G, 16B, the 2-byte value for each R/G/B component is stored as big-endian.
    //RGB48LE,   // 35 packed RGB 16:16:16, 48bpp, 16R, 16G, 16B, the 2-byte value for each R/G/B component is stored as little-endian.

    //RGB565BE,  // 36 packed RGB 5:6:5, 16bpp, (msb)   5R 6G 5B(lsb), big-endian.
    //RGB565LE,  // 37 packed RGB 5:6:5, 16bpp, (msb)   5R 6G 5B(lsb), little-endian.
    //RGB555BE,  // 38 packed RGB 5:5:5, 16bpp, (msb)1X 5R 5G 5B(lsb), big-endian   , X=unused/undefined.
    //RGB555LE,  // 39 packed RGB 5:5:5, 16bpp, (msb)1X 5R 5G 5B(lsb), little-endian, X=unused/undefined.

    //BGR565BE,  // 40 packed BGR 5:6:5, 16bpp, (msb)   5B 6G 5R(lsb), big-endian.
    //BGR565LE,  // 41 packed BGR 5:6:5, 16bpp, (msb)   5B 6G 5R(lsb), little-endian.
    //BGR555BE,  // 42 packed BGR 5:5:5, 16bpp, (msb)1X 5B 5G 5R(lsb), big-endian   , X=unused/undefined.
    //BGR555LE,  // 43 packed BGR 5:5:5, 16bpp, (msb)1X 5B 5G 5R(lsb), little-endian, X=unused/undefined.

    // VAAPI, // 44.

    //YUV420P16LE,  // 45 planar YUV 4:2:0, 24bpp, (1 Cr & Cb sample per 2x2 Y samples), little-endian.
    //YUV420P16BE,  // 46 planar YUV 4:2:0, 24bpp, (1 Cr & Cb sample per 2x2 Y samples), big-endian.
    //YUV422P16LE,  // 47 planar YUV 4:2:2, 32bpp, (1 Cr & Cb sample per 2x1 Y samples), little-endian.
    //YUV422P16BE,  // 48 planar YUV 4:2:2, 32bpp, (1 Cr & Cb sample per 2x1 Y samples), big-endian.
    //YUV444P16LE,  // 49 planar YUV 4:4:4, 48bpp, (1 Cr & Cb sample per 1x1 Y samples), little-endian.
    //YUV444P16BE,  // 50 planar YUV 4:4:4, 48bpp, (1 Cr & Cb sample per 1x1 Y samples), big-endian.
    //DXVA2_VLD,    // 51 HW decoding through DXVA2, Picture.data[3] contains a LPDIRECT3DSURFACE9 pointer.

    //RGB444LE,  // 52 packed RGB 4:4:4, 16bpp, (msb)4X 4R 4G 4B(lsb), little-endian, X=unused/undefined.
    //RGB444BE,  // 53 packed RGB 4:4:4, 16bpp, (msb)4X 4R 4G 4B(lsb), big-endian,    X=unused/undefined.
    //BGR444LE,  // 54 packed BGR 4:4:4, 16bpp, (msb)4X 4B 4G 4R(lsb), little-endian, X=unused/undefined.
    //BGR444BE,  // 55 packed BGR 4:4:4, 16bpp, (msb)4X 4B 4G 4R(lsb), big-endian,    X=unused/undefined.
    //YA8,       // 56 8 bits gray, 8 bits alpha.

    //Y400A, // 57.
    //GRAY8A // 58.

    //BGR48BE,   // 59 packed RGB 16:16:16, 48bpp, 16B, 16G, 16R, the 2-byte value for each R/G/B component is stored as big-endian.
    //BGR48LE,   // 60 packed RGB 16:16:16, 48bpp, 16B, 16G, 16R, the 2-byte value for each R/G/B component is stored as little-endian.

    // The following 12 formats have the disadvantage of needing 1 format for each bit depth.
    // Notice that each 9/10 bits sample is stored in 16 bits with extra padding.
    // If you want to support multiple bit depths, then using YUV420P16* with the bpp stored separately is better.
    YUV420P9BE, // 61 planar YUV 4:2:0, 13.5bpp, (1 Cr & Cb sample per 2x2 Y samples), big-endian
    YUV420P9LE, // 62 planar YUV 4:2:0, 13.5bpp, (1 Cr & Cb sample per 2x2 Y samples), little-endian
    YUV420P10BE, // 63 planar YUV 4:2:0, 15bpp, (1 Cr & Cb sample per 2x2 Y samples), big-endian
    YUV420P10LE, // 64 planar YUV 4:2:0, 15bpp, (1 Cr & Cb sample per 2x2 Y samples), little-endian
    YUV422P10BE, // 65 planar YUV 4:2:2, 20bpp, (1 Cr & Cb sample per 2x1 Y samples), big-endian
    YUV422P10LE, // 66 planar YUV 4:2:2, 20bpp, (1 Cr & Cb sample per 2x1 Y samples), little-endian
    YUV444P9BE, // 67 planar YUV 4:4:4, 27bpp, (1 Cr & Cb sample per 1x1 Y samples), big-endian
    YUV444P9LE, // 68 planar YUV 4:4:4, 27bpp, (1 Cr & Cb sample per 1x1 Y samples), little-endian
    YUV444P10BE, // 69 planar YUV 4:4:4, 30bpp, (1 Cr & Cb sample per 1x1 Y samples), big-endian
    YUV444P10LE, // 70 planar YUV 4:4:4, 30bpp, (1 Cr & Cb sample per 1x1 Y samples), little-endian
    YUV422P9BE, // 71 planar YUV 4:2:2, 18bpp, (1 Cr & Cb sample per 2x1 Y samples), big-endian
    YUV422P9LE, // 72 planar YUV 4:2:2, 18bpp, (1 Cr & Cb sample per 2x1 Y samples), little-endian
    GBRP,       // 73 planar GBR 4:4:4 24bpp.
    //GBR24P // 74.
    GBRP9BE,  // 75 planar GBR 4:4:4 27bpp, big-endian.
    GBRP9LE,  // 76 planar GBR 4:4:4 27bpp, little-endian.
    GBRP10BE, // 77 planar GBR 4:4:4 30bpp, big-endian.
    GBRP10LE, // 78 planar GBR 4:4:4 30bpp, little-endian.
    //GBRP16BE, // 79 planar GBR 4:4:4 48bpp, big-endian.
    //GBRP16LE, // 80 planar GBR 4:4:4 48bpp, little-endian.
    //YUVA422P,  // 81 planar YUV 4:2:2 24bpp, (1 Cr & Cb sample per 2x1 Y & A samples).
    //YUVA444P,  // 82 planar YUV 4:4:4 32bpp, (1 Cr & Cb sample per 1x1 Y & A samples).
    //YUVA420P9BE,  // 83 planar YUV 4:2:0 22.5bpp, (1 Cr & Cb sample per 2x2 Y & A samples), big-endian.
    //YUVA420P9LE,  // 84 planar YUV 4:2:0 22.5bpp, (1 Cr & Cb sample per 2x2 Y & A samples), little-endian.
    //YUVA422P9BE,  // 85 planar YUV 4:2:2 27bpp, (1 Cr & Cb sample per 2x1 Y & A samples), big-endian.
    //YUVA422P9LE,  // 86 planar YUV 4:2:2 27bpp, (1 Cr & Cb sample per 2x1 Y & A samples), little-endian.
    //YUVA444P9BE,  // 87 planar YUV 4:4:4 36bpp, (1 Cr & Cb sample per 1x1 Y & A samples), big-endian.
    //YUVA444P9LE,  // 88 planar YUV 4:4:4 36bpp, (1 Cr & Cb sample per 1x1 Y & A samples), little-endian.
    //YUVA420P10BE, // 89 planar YUV 4:2:0 25bpp, (1 Cr & Cb sample per 2x2 Y & A samples, big-endian).
    //YUVA420P10LE, // 90 planar YUV 4:2:0 25bpp, (1 Cr & Cb sample per 2x2 Y & A samples, little-endian).
    //YUVA422P10BE, // 91 planar YUV 4:2:2 30bpp, (1 Cr & Cb sample per 2x1 Y & A samples, big-endian).
    //YUVA422P10LE, // 92 planar YUV 4:2:2 30bpp, (1 Cr & Cb sample per 2x1 Y & A samples, little-endian).
    //YUVA444P10BE, // 93 planar YUV 4:4:4 40bpp, (1 Cr & Cb sample per 1x1 Y & A samples, big-endian).
    //YUVA444P10LE, // 94 planar YUV 4:4:4 40bpp, (1 Cr & Cb sample per 1x1 Y & A samples, little-endian).
    //YUVA420P16BE, // 95 planar YUV 4:2:0 40bpp, (1 Cr & Cb sample per 2x2 Y & A samples, big-endian).
    //YUVA420P16LE, // 96 planar YUV 4:2:0 40bpp, (1 Cr & Cb sample per 2x2 Y & A samples, little-endian).
    //YUVA422P16BE, // 98 planar YUV 4:2:2 48bpp, (1 Cr & Cb sample per 2x1 Y & A samples, big-endian).
    //YUVA422P16LE, // 99 planar YUV 4:2:2 48bpp, (1 Cr & Cb sample per 2x1 Y & A samples, little-endian).
    //YUVA444P16BE, // 100 planar YUV 4:4:4 64bpp, (1 Cr & Cb sample per 1x1 Y & A samples, big-endian).
    //YUVA444P16LE, // 101 planar YUV 4:4:4 64bpp, (1 Cr & Cb sample per 1x1 Y & A samples, little-endian).

    // VDPAU // 102.

    //XYZ12LE, // 103 packed XYZ 4:4:4, 36 bpp, (msb) 12X, 12Y, 12Z (lsb), the 2-byte value for each X/Y/Z is stored as little-endian, the 4 lower bits are set to 0
    //XYZ12BE, // 104 packed XYZ 4:4:4, 36 bpp, (msb) 12X, 12Y, 12Z (lsb), the 2-byte value for each X/Y/Z is stored as big-endian, the 4 lower bits are set to 0
    //NV16,    // 105 interleaved chroma YUV 4:2:2, 16bpp, (1 Cr & Cb sample per 2x1 Y samples)
    //NV20LE,  // 106 interleaved chroma YUV 4:2:2, 20bpp, (1 Cr & Cb sample per 2x1 Y samples), little-endian
    //NV20BE,  // 107 interleaved chroma YUV 4:2:2, 20bpp, (1 Cr & Cb sample per 2x1 Y samples), big-endian

    //RGBA64BE, // 108 packed RGBA 16:16:16:16, 64bpp, 16R, 16G, 16B, 16A, the 2-byte value for each R/G/B/A component is stored as big-endian
    //RGBA64LE, // 109 packed RGBA 16:16:16:16, 64bpp, 16R, 16G, 16B, 16A, the 2-byte value for each R/G/B/A component is stored as little-endian
    //BGRA64BE, // 110 packed RGBA 16:16:16:16, 64bpp, 16B, 16G, 16R, 16A, the 2-byte value for each R/G/B/A component is stored as big-endian
    //BGRA64LE, // 111 packed RGBA 16:16:16:16, 64bpp, 16B, 16G, 16R, 16A, the 2-byte value for each R/G/B/A component is stored as little-endian

    //YVYU422, // 112 packed YUV 4:2:2, 16bpp, Y0 Cr Y1 Cb

    //YA16BE, // 113 16 bits gray, 16 bits alpha (big-endian)
    //YA16LE, // 114 16 bits gray, 16 bits alpha (little-endian)

    //GBRAP,     // 115 planar GBRA 4:4:4:4 32bpp
    //GBRAP16BE, // 116 planar GBRA 4:4:4:4 64bpp, big-endian
    //GBRAP16LE, // 117 planar GBRA 4:4:4:4 64bpp, little-endian

    //QSV,        // 118.
    //MMAL,       // 119.
    //D3D11VA_VLD // 120.
    //CUDA        // 121.

    //0RGB, // 122 packed RGB 8:8:8, 32bpp, XRGBXRGB...   X=unused/undefined.
    //RGB0, // 123 packed RGB 8:8:8, 32bpp, RGBXRGBX...   X=unused/undefined.
    //0BGR, // 124 packed BGR 8:8:8, 32bpp, XBGRXBGR...   X=unused/undefined.
    //BGR0, // 125 packed BGR 8:8:8, 32bpp, BGRXBGRX...   X=unused/undefined.
    //
    YUV420P12BE, // 126 planar YUV 4:2:0,18bpp, (1 Cr & Cb sample per 2x2 Y samples), big-endian.
    YUV420P12LE, // 127 planar YUV 4:2:0,18bpp, (1 Cr & Cb sample per 2x2 Y samples), little-endian.
    YUV420P14BE, // 128 planar YUV 4:2:0,21bpp, (1 Cr & Cb sample per 2x2 Y samples), big-endian.
    YUV420P14LE, // 129 planar YUV 4:2:0,21bpp, (1 Cr & Cb sample per 2x2 Y samples), little-endian.
    YUV422P12BE, // 130 planar YUV 4:2:2,24bpp, (1 Cr & Cb sample per 2x1 Y samples), big-endian.
    YUV422P12LE, // 131 planar YUV 4:2:2,24bpp, (1 Cr & Cb sample per 2x1 Y samples), little-endian.
    YUV422P14BE, // 132 planar YUV 4:2:2,28bpp, (1 Cr & Cb sample per 2x1 Y samples), big-endian.
    YUV422P14LE, // 133 planar YUV 4:2:2,28bpp, (1 Cr & Cb sample per 2x1 Y samples), little-endian.
    YUV444P12BE, // 134 planar YUV 4:4:4,36bpp, (1 Cr & Cb sample per 1x1 Y samples), big-endian.
    YUV444P12LE, // 135 planar YUV 4:4:4,36bpp, (1 Cr & Cb sample per 1x1 Y samples), little-endian.
    YUV444P14BE, // 136 planar YUV 4:4:4,42bpp, (1 Cr & Cb sample per 1x1 Y samples), big-endian.
    YUV444P14LE, // 137 planar YUV 4:4:4,42bpp, (1 Cr & Cb sample per 1x1 Y samples), little-endian.
    GBRP12BE,    // 138 planar GBR 4:4:4 36bpp, big-endian.
    GBRP12LE,    // 139 planar GBR 4:4:4 36bpp, little-endian.
    GBRP14BE,    // 140 planar GBR 4:4:4 42bpp, big-endian.
    GBRP14LE,    // 141 planar GBR 4:4:4 42bpp, little-endian.

                 //YUVJ411P,    // 142 planar YUV 4:1:1, 12bpp, (1 Cr & Cb sample per 4x1 Y samples) full scale (JPEG), deprecated in favor of AV_PIX_FMT_YUV411P and setting color_range.
}

#[cfg(target_endian = "big")]
macro_rules! av_pix_fmt_ne {
    ($name:ident, $be:ident, $le:ident) => {
        pub const $name: PixelFormat = PixelFormat::$be;
    };
}
#[cfg(target_endian = "little")]
macro_rules! av_pix_fmt_ne {
    ($name:ident, $be:ident, $le:ident) => {
        pub const $name: PixelFormat = PixelFormat::$le;
    };
}

av_pix_fmt_ne!(PIX_FMT_YUV420P9, YUV420P9BE, YUV420P9LE);
av_pix_fmt_ne!(PIX_FMT_YUV422P9, YUV422P9BE, YUV422P9LE);
av_pix_fmt_ne!(PIX_FMT_YUV444P9, YUV444P9BE, YUV444P9LE);
av_pix_fmt_ne!(PIX_FMT_YUV420P10, YUV420P10BE, YUV420P10LE);
av_pix_fmt_ne!(PIX_FMT_YUV422P10, YUV422P10BE, YUV422P10LE);
av_pix_fmt_ne!(PIX_FMT_YUV444P10, YUV444P10BE, YUV444P10LE);
av_pix_fmt_ne!(PIX_FMT_YUV420P12, YUV420P12BE, YUV420P12LE);
av_pix_fmt_ne!(PIX_FMT_YUV422P12, YUV422P12BE, YUV422P12LE);
av_pix_fmt_ne!(PIX_FMT_YUV444P12, YUV444P12BE, YUV444P12LE);
av_pix_fmt_ne!(PIX_FMT_YUV420P14, YUV420P14BE, YUV420P14LE);
av_pix_fmt_ne!(PIX_FMT_YUV422P14, YUV422P14BE, YUV422P14LE);
av_pix_fmt_ne!(PIX_FMT_YUV444P14, YUV444P14BE, YUV444P14LE);

av_pix_fmt_ne!(PIX_FMT_GBRP9, GBRP9BE, GBRP9LE);
av_pix_fmt_ne!(PIX_FMT_GBRP10, GBRP10BE, GBRP10LE);
av_pix_fmt_ne!(PIX_FMT_GBRP12, GBRP12BE, GBRP12LE);
av_pix_fmt_ne!(PIX_FMT_GBRP14, GBRP14BE, GBRP14LE);

impl PixelFormat {
    #[must_use]
    pub fn name(&self) -> &str {
        match self {
            PixelFormat::YUV420P => "yuv420p",
            PixelFormat::RGB24 => "rgb24",
            PixelFormat::YUV422P => "yuv422p",
            PixelFormat::YUV444P => "yuv444p",
            PixelFormat::GRAY8 => "gray8",
            PixelFormat::YUVJ420P => "yuvj420p",
            PixelFormat::YUVJ422P => "yuvj422p",
            PixelFormat::YUVJ444P => "yuvj444p",
            PixelFormat::YUVA420P => "yuva420p",
            PixelFormat::YUV420P9BE => "yuv420p9be",
            PixelFormat::YUV420P9LE => "yuv420p9le",
            PixelFormat::YUV420P10BE => "yuv420p10be",
            PixelFormat::YUV420P10LE => "yuv420p10le",
            PixelFormat::YUV422P10BE => "yuv422p10be",
            PixelFormat::YUV422P10LE => "yuv422p10le",
            PixelFormat::YUV444P9BE => "yuv444p9be",
            PixelFormat::YUV444P9LE => "yuv444p9le",
            PixelFormat::YUV444P10BE => "yuv444p10be",
            PixelFormat::YUV444P10LE => "yuv444p10le",
            PixelFormat::YUV422P9BE => "yuv422p9be",
            PixelFormat::YUV422P9LE => "yuv422p9le",
            PixelFormat::GBRP => "gbrp",
            PixelFormat::GBRP9BE => "gbrp9be",
            PixelFormat::GBRP9LE => "gbrp9le",
            PixelFormat::GBRP10BE => "gbrp10be",
            PixelFormat::GBRP10LE => "gbrp10le",
            PixelFormat::YUV420P12BE => "yuv420p12be",
            PixelFormat::YUV420P12LE => "yuv420p12le",
            PixelFormat::YUV420P14BE => "yuv420p14be",
            PixelFormat::YUV420P14LE => "yuv420p14le",
            PixelFormat::YUV422P12BE => "yuv422p12be",
            PixelFormat::YUV422P12LE => "yuv422p12le",
            PixelFormat::YUV422P14BE => "yuv422p14be",
            PixelFormat::YUV422P14LE => "yuv422p14le",
            PixelFormat::YUV444P12BE => "yuv444p12be",
            PixelFormat::YUV444P12LE => "yuv444p12le",
            PixelFormat::YUV444P14BE => "yuv444p14be",
            PixelFormat::YUV444P14LE => "yuv444p14le",
            PixelFormat::GBRP12BE => "gbrp12be",
            PixelFormat::GBRP12LE => "gbrp12le",
            PixelFormat::GBRP14BE => "gbrp14be",
            PixelFormat::GBRP14LE => "gbrp14le",
        }
    }

    // Amount to shift the luma width right to find the chroma width.
    // For YV12 this is 1 for example.
    // chroma_width = AV_CEIL_RSHIFT(luma_width, log2_chroma_w)
    // The note above is needed to ensure rounding up.
    // This value only refers to the chroma components.
    #[must_use]
    #[allow(clippy::match_same_arms)]
    pub fn log2_chroma_w(&self) -> u8 {
        match self {
            PixelFormat::YUV420P => 1,
            PixelFormat::RGB24 => 0,
            PixelFormat::YUV422P => 1,
            PixelFormat::YUV444P => 0,
            PixelFormat::GRAY8 => 0,
            PixelFormat::YUVJ420P => 1,
            PixelFormat::YUVJ422P => 1,
            PixelFormat::YUVJ444P => 0,
            PixelFormat::YUVA420P => 1,
            PixelFormat::YUV420P9BE => 1,
            PixelFormat::YUV420P9LE => 1,
            PixelFormat::YUV420P10BE => 1,
            PixelFormat::YUV420P10LE => 1,
            PixelFormat::YUV422P10BE => 1,
            PixelFormat::YUV422P10LE => 1,
            PixelFormat::YUV444P9BE => 0,
            PixelFormat::YUV444P9LE => 0,
            PixelFormat::YUV444P10BE => 0,
            PixelFormat::YUV444P10LE => 0,
            PixelFormat::YUV422P9BE => 1,
            PixelFormat::YUV422P9LE => 1,
            PixelFormat::GBRP => 0,
            PixelFormat::GBRP9BE => 0,
            PixelFormat::GBRP9LE => 0,
            PixelFormat::GBRP10BE => 0,
            PixelFormat::GBRP10LE => 0,
            PixelFormat::YUV420P12BE => 1,
            PixelFormat::YUV420P12LE => 1,
            PixelFormat::YUV420P14BE => 1,
            PixelFormat::YUV420P14LE => 1,
            PixelFormat::YUV422P12BE => 1,
            PixelFormat::YUV422P12LE => 1,
            PixelFormat::YUV422P14BE => 1,
            PixelFormat::YUV422P14LE => 1,
            PixelFormat::YUV444P12BE => 0,
            PixelFormat::YUV444P12LE => 0,
            PixelFormat::YUV444P14BE => 0,
            PixelFormat::YUV444P14LE => 0,
            PixelFormat::GBRP12BE => 0,
            PixelFormat::GBRP12LE => 0,
            PixelFormat::GBRP14BE => 0,
            PixelFormat::GBRP14LE => 0,
        }
    }

    // Amount to shift the luma height right to find the chroma height.
    // For YV12 this is 1 for example.
    //    chroma_height= AV_CEIL_RSHIFT(luma_height, log2_chroma_h)
    // The note above is needed to ensure rounding up.
    // This value only refers to the chroma components.
    #[must_use]
    #[allow(clippy::match_same_arms)]
    pub fn log2_chroma_h(&self) -> u8 {
        match self {
            PixelFormat::YUV420P => 1,
            PixelFormat::RGB24 => 0,
            PixelFormat::YUV422P => 0,
            PixelFormat::YUV444P => 0,
            PixelFormat::GRAY8 => 0,
            PixelFormat::YUVJ420P => 1,
            PixelFormat::YUVJ422P => 0,
            PixelFormat::YUVJ444P => 0,
            PixelFormat::YUVA420P => 1,
            PixelFormat::YUV420P9BE => 1,
            PixelFormat::YUV420P9LE => 1,
            PixelFormat::YUV420P10BE => 1,
            PixelFormat::YUV420P10LE => 1,
            PixelFormat::YUV422P10BE => 0,
            PixelFormat::YUV422P10LE => 0,
            PixelFormat::YUV444P9BE => 0,
            PixelFormat::YUV444P9LE => 0,
            PixelFormat::YUV444P10BE => 0,
            PixelFormat::YUV444P10LE => 0,
            PixelFormat::YUV422P9BE => 0,
            PixelFormat::YUV422P9LE => 0,
            PixelFormat::GBRP => 0,
            PixelFormat::GBRP9BE => 0,
            PixelFormat::GBRP9LE => 0,
            PixelFormat::GBRP10BE => 0,
            PixelFormat::GBRP10LE => 0,
            PixelFormat::YUV420P12BE => 1,
            PixelFormat::YUV420P12LE => 1,
            PixelFormat::YUV420P14BE => 1,
            PixelFormat::YUV420P14LE => 1,
            PixelFormat::YUV422P12BE => 0,
            PixelFormat::YUV422P12LE => 0,
            PixelFormat::YUV422P14BE => 0,
            PixelFormat::YUV422P14LE => 0,
            PixelFormat::YUV444P12BE => 0,
            PixelFormat::YUV444P12LE => 0,
            PixelFormat::YUV444P14BE => 0,
            PixelFormat::YUV444P14LE => 0,
            PixelFormat::GBRP12BE => 0,
            PixelFormat::GBRP12LE => 0,
            PixelFormat::GBRP14BE => 0,
            PixelFormat::GBRP14LE => 0,
        }
    }

    #[must_use]
    #[allow(clippy::match_same_arms)]
    pub fn flags(&self) -> u64 {
        match self {
            PixelFormat::YUV420P => PIX_FMT_FLAG_PLANAR,
            PixelFormat::RGB24 => PIX_FMT_FLAG_RGB,
            PixelFormat::YUV422P => PIX_FMT_FLAG_PLANAR,
            PixelFormat::YUV444P => PIX_FMT_FLAG_PLANAR,
            PixelFormat::GRAY8 => 0,
            PixelFormat::YUVJ420P => PIX_FMT_FLAG_PLANAR,
            PixelFormat::YUVJ422P => PIX_FMT_FLAG_PLANAR,
            PixelFormat::YUVJ444P => PIX_FMT_FLAG_PLANAR,
            PixelFormat::YUVA420P => PIX_FMT_FLAG_PLANAR | PIX_FMT_FLAG_ALPHA,
            PixelFormat::YUV420P9BE => PIX_FMT_FLAG_BE | PIX_FMT_FLAG_PLANAR | PIX_FMT_FLAG_ALPHA,
            PixelFormat::YUV420P9LE => PIX_FMT_FLAG_PLANAR | PIX_FMT_FLAG_ALPHA,
            PixelFormat::YUV420P10BE => PIX_FMT_FLAG_BE | PIX_FMT_FLAG_PLANAR | PIX_FMT_FLAG_ALPHA,
            PixelFormat::YUV420P10LE => PIX_FMT_FLAG_PLANAR | PIX_FMT_FLAG_ALPHA,
            PixelFormat::YUV422P10BE => PIX_FMT_FLAG_BE | PIX_FMT_FLAG_PLANAR | PIX_FMT_FLAG_ALPHA,
            PixelFormat::YUV422P10LE => PIX_FMT_FLAG_PLANAR | PIX_FMT_FLAG_ALPHA,
            PixelFormat::YUV444P9BE => PIX_FMT_FLAG_BE | PIX_FMT_FLAG_PLANAR,
            PixelFormat::YUV444P9LE => PIX_FMT_FLAG_PLANAR,
            PixelFormat::YUV444P10BE => PIX_FMT_FLAG_BE | PIX_FMT_FLAG_PLANAR | PIX_FMT_FLAG_ALPHA,
            PixelFormat::YUV444P10LE => PIX_FMT_FLAG_PLANAR | PIX_FMT_FLAG_ALPHA,
            PixelFormat::YUV422P9BE => PIX_FMT_FLAG_BE | PIX_FMT_FLAG_PLANAR | PIX_FMT_FLAG_ALPHA,
            PixelFormat::YUV422P9LE => PIX_FMT_FLAG_PLANAR | PIX_FMT_FLAG_ALPHA,
            PixelFormat::GBRP => PIX_FMT_FLAG_PLANAR | PIX_FMT_FLAG_RGB,
            PixelFormat::GBRP9BE => PIX_FMT_FLAG_BE | PIX_FMT_FLAG_PLANAR | PIX_FMT_FLAG_RGB,
            PixelFormat::GBRP9LE => PIX_FMT_FLAG_PLANAR | PIX_FMT_FLAG_RGB,
            PixelFormat::GBRP10BE => PIX_FMT_FLAG_BE | PIX_FMT_FLAG_PLANAR | PIX_FMT_FLAG_RGB,
            PixelFormat::GBRP10LE => PIX_FMT_FLAG_PLANAR | PIX_FMT_FLAG_RGB,
            PixelFormat::YUV420P12BE => PIX_FMT_FLAG_BE | PIX_FMT_FLAG_PLANAR,
            PixelFormat::YUV420P12LE => PIX_FMT_FLAG_PLANAR,
            PixelFormat::YUV420P14BE => PIX_FMT_FLAG_BE | PIX_FMT_FLAG_PLANAR,
            PixelFormat::YUV420P14LE => PIX_FMT_FLAG_PLANAR,
            PixelFormat::YUV422P12BE => PIX_FMT_FLAG_BE | PIX_FMT_FLAG_PLANAR | PIX_FMT_FLAG_ALPHA,
            PixelFormat::YUV422P12LE => PIX_FMT_FLAG_PLANAR | PIX_FMT_FLAG_ALPHA,
            PixelFormat::YUV422P14BE => PIX_FMT_FLAG_BE | PIX_FMT_FLAG_PLANAR,
            PixelFormat::YUV422P14LE => PIX_FMT_FLAG_PLANAR,
            PixelFormat::YUV444P12BE => PIX_FMT_FLAG_BE | PIX_FMT_FLAG_PLANAR | PIX_FMT_FLAG_ALPHA,
            PixelFormat::YUV444P12LE => PIX_FMT_FLAG_PLANAR | PIX_FMT_FLAG_ALPHA,
            PixelFormat::YUV444P14BE => PIX_FMT_FLAG_BE | PIX_FMT_FLAG_PLANAR,
            PixelFormat::YUV444P14LE => PIX_FMT_FLAG_PLANAR,
            PixelFormat::GBRP12BE => PIX_FMT_FLAG_BE | PIX_FMT_FLAG_PLANAR | PIX_FMT_FLAG_RGB,
            PixelFormat::GBRP12LE => PIX_FMT_FLAG_PLANAR | PIX_FMT_FLAG_RGB,
            PixelFormat::GBRP14BE => PIX_FMT_FLAG_BE | PIX_FMT_FLAG_PLANAR | PIX_FMT_FLAG_RGB,
            PixelFormat::GBRP14LE => PIX_FMT_FLAG_PLANAR | PIX_FMT_FLAG_RGB,
        }
    }

    // Parameters that describe how pixels are packed.
    // If the format has 1 or 2 components, then luma is 0.
    // If the format has 3 or 4 components:
    // if the RGB flag is set then 0 is red, 1 is green and 2 is blue;
    // otherwise 0 is luma, 1 is chroma-U and 2 is chroma-V.
    // If present, the Alpha channel is always the last component.
    #[must_use]
    #[allow(clippy::match_same_arms, clippy::too_many_lines)]
    pub fn comps(&self) -> &[ComponentDescriptor] {
        const fn cd(plane: u8, step: u8, offset: u8, shift: u8, depth: u8) -> ComponentDescriptor {
            ComponentDescriptor {
                plane,
                step,
                offset,
                shift,
                depth,
            }
        }
        const GRAY8: [ComponentDescriptor; 1] = [cd(0, 1, 0, 0, 8)];
        const RGB: [ComponentDescriptor; 3] = [
            cd(0, 3, 0, 0, 8), // R.
            cd(0, 3, 1, 0, 8), // G.
            cd(0, 3, 2, 0, 8), // B.
        ];
        const YUVP: [ComponentDescriptor; 3] = [
            cd(0, 1, 0, 0, 8), // Y.
            cd(1, 1, 0, 0, 8), // U.
            cd(2, 1, 0, 0, 8), // V.
        ];
        const YUVA: [ComponentDescriptor; 4] = [
            cd(0, 1, 0, 0, 8), // Y.
            cd(1, 1, 0, 0, 8), // Y.
            cd(2, 1, 0, 0, 8), // Y.
            cd(3, 1, 0, 0, 8), // Y.
        ];
        const YUVP9: [ComponentDescriptor; 3] = [
            cd(0, 2, 0, 0, 9), // Y.
            cd(1, 2, 0, 0, 9), // U.
            cd(2, 2, 0, 0, 9), // V.
        ];
        const YUVP10: [ComponentDescriptor; 3] = [
            cd(0, 2, 0, 0, 10), // Y.
            cd(1, 2, 0, 0, 10), // U.
            cd(2, 2, 0, 0, 10), // V.
        ];
        const YUVP12: [ComponentDescriptor; 3] = [
            cd(0, 2, 0, 0, 12), // Y.
            cd(1, 2, 0, 0, 12), // U.
            cd(2, 2, 0, 0, 12), // V.
        ];
        const YUVP14: [ComponentDescriptor; 3] = [
            cd(0, 2, 0, 0, 14), // Y.
            cd(1, 2, 0, 0, 14), // U.
            cd(2, 2, 0, 0, 14), // V.
        ];

        const GBRP: [ComponentDescriptor; 3] = [
            cd(2, 1, 0, 0, 8), // R.
            cd(0, 1, 0, 0, 8), // G.
            cd(1, 1, 0, 0, 8), // B.
        ];
        const GBRP9: [ComponentDescriptor; 3] = [
            cd(2, 2, 0, 0, 9), // R.
            cd(0, 2, 0, 0, 9), // G.
            cd(1, 2, 0, 0, 9), // B.
        ];
        const GBRP10: [ComponentDescriptor; 3] = [
            cd(2, 2, 0, 0, 10), // R.
            cd(0, 2, 0, 0, 10), // G.
            cd(1, 2, 0, 0, 10), // B.
        ];
        const GBRP12: [ComponentDescriptor; 3] = [
            cd(2, 2, 0, 0, 12), // R.
            cd(0, 2, 0, 0, 12), // G.
            cd(1, 2, 0, 0, 12), // B.
        ];
        const GBRP14: [ComponentDescriptor; 3] = [
            cd(2, 2, 0, 0, 14), // R.
            cd(0, 2, 0, 0, 14), // G.
            cd(1, 2, 0, 0, 14), // B.
        ];

        match self {
            PixelFormat::YUV420P => &YUVP,
            PixelFormat::RGB24 => &RGB,
            PixelFormat::YUV422P => &YUVP,
            PixelFormat::YUV444P => &YUVP,
            PixelFormat::GRAY8 => &GRAY8,
            PixelFormat::YUVJ420P => &YUVP,
            PixelFormat::YUVJ422P => &YUVP,
            PixelFormat::YUVJ444P => &YUVP,
            PixelFormat::YUVA420P => &YUVA,
            PixelFormat::YUV420P9BE => &YUVP9,
            PixelFormat::YUV420P9LE => &YUVP9,
            PixelFormat::YUV420P10BE => &YUVP10,
            PixelFormat::YUV420P10LE => &YUVP10,
            PixelFormat::YUV422P10BE => &YUVP10,
            PixelFormat::YUV422P10LE => &YUVP10,
            PixelFormat::YUV444P9BE => &YUVP9,
            PixelFormat::YUV444P9LE => &YUVP9,
            PixelFormat::YUV444P10BE => &YUVP10,
            PixelFormat::YUV444P10LE => &YUVP10,
            PixelFormat::YUV422P9BE => &YUVP9,
            PixelFormat::YUV422P9LE => &YUVP9,
            PixelFormat::GBRP => &GBRP,
            PixelFormat::GBRP9BE => &GBRP9,
            PixelFormat::GBRP9LE => &GBRP9,
            PixelFormat::GBRP10BE => &GBRP10,
            PixelFormat::GBRP10LE => &GBRP10,
            PixelFormat::YUV420P12BE => &YUVP12,
            PixelFormat::YUV420P12LE => &YUVP12,
            PixelFormat::YUV420P14BE => &YUVP14,
            PixelFormat::YUV420P14LE => &YUVP14,
            PixelFormat::YUV422P12BE => &YUVP12,
            PixelFormat::YUV422P12LE => &YUVP12,
            PixelFormat::YUV422P14BE => &YUVP14,
            PixelFormat::YUV422P14LE => &YUVP14,
            PixelFormat::YUV444P12BE => &YUVP12,
            PixelFormat::YUV444P12LE => &YUVP12,
            PixelFormat::YUV444P14BE => &YUVP14,
            PixelFormat::YUV444P14LE => &YUVP14,
            PixelFormat::GBRP12BE => &GBRP12,
            PixelFormat::GBRP12LE => &GBRP12,
            PixelFormat::GBRP14BE => &GBRP14,
            PixelFormat::GBRP14LE => &GBRP14,
        }
    }

    #[must_use]
    pub fn num_comps(&self) -> u8 {
        u8::try_from(self.comps().len()).expect("comps len to fit u8")
    }

    // Return the number of bits per pixel used by the pixel format
    // described by pixdesc. Note that this is not the same as the number
    // of bits per sample.
    //
    // The returned number of bits refers to the number of bits actually
    // used for storing the pixel information, that is padding bits are
    // not counted.
    #[must_use]
    pub fn bits_per_pixel(&self) -> u8 {
        let mut bits = 0;
        let log2_pixels = self.log2_chroma_w() + self.log2_chroma_h();

        for c in 0..self.comps().len() {
            let s = if c == 1 || c == 2 { 0 } else { log2_pixels };
            bits += self.comps()[c].depth << s;
        }
        /*for (c = 0; c < pixdesc->nb_components; c++) {
            int s = c == 1 || c == 2 ? 0 : log2_pixels;
            bits += pixdesc->comp[c].depth << s;
        }*/

        bits >> log2_pixels
        //return bits >> log2_pixels;
    }

    // Return the number of bits per pixel for the pixel format
    // described by pixdesc, including any padding or unused bits.
    #[must_use]
    pub fn padded_bits_per_pixel(&self) -> u8 {
        let mut bits = 0;
        let log2_pixels = self.log2_chroma_w() + self.log2_chroma_h();

        for (c, comp) in self.comps().iter().enumerate() {
            if c == 1 || c == 2 {
                bits += comp.step;
            } else {
                bits += comp.step << log2_pixels;
            }
        }

        if (self.flags() & PIX_FMT_FLAG_BITSTREAM) == 0 {
            bits *= 8;
        }

        bits >> log2_pixels
    }

    #[must_use]
    pub fn as_i32(&self) -> i32 {
        match self {
            PixelFormat::YUV420P => 0,
            PixelFormat::RGB24 => 2,
            PixelFormat::YUV422P => 4,
            PixelFormat::YUV444P => 5,
            PixelFormat::GRAY8 => 8,
            PixelFormat::YUVJ420P => 12,
            PixelFormat::YUVJ422P => 13,
            PixelFormat::YUVJ444P => 14,
            PixelFormat::YUVA420P => 33,
            PixelFormat::YUV420P9BE => 61,
            PixelFormat::YUV420P9LE => 62,
            PixelFormat::YUV420P10BE => 63,
            PixelFormat::YUV420P10LE => 64,
            PixelFormat::YUV422P10BE => 65,
            PixelFormat::YUV422P10LE => 66,
            PixelFormat::YUV444P9BE => 67,
            PixelFormat::YUV444P9LE => 68,
            PixelFormat::YUV444P10BE => 69,
            PixelFormat::YUV444P10LE => 70,
            PixelFormat::YUV422P9BE => 71,
            PixelFormat::YUV422P9LE => 72,
            PixelFormat::GBRP => 73,
            PixelFormat::GBRP9BE => 75,
            PixelFormat::GBRP9LE => 76,
            PixelFormat::GBRP10BE => 77,
            PixelFormat::GBRP10LE => 78,
            PixelFormat::YUV420P12BE => 126,
            PixelFormat::YUV420P12LE => 127,
            PixelFormat::YUV420P14BE => 128,
            PixelFormat::YUV420P14LE => 129,
            PixelFormat::YUV422P12BE => 130,
            PixelFormat::YUV422P12LE => 131,
            PixelFormat::YUV422P14BE => 132,
            PixelFormat::YUV422P14LE => 133,
            PixelFormat::YUV444P12BE => 134,
            PixelFormat::YUV444P12LE => 135,
            PixelFormat::YUV444P14BE => 136,
            PixelFormat::YUV444P14LE => 137,
            PixelFormat::GBRP12BE => 138,
            PixelFormat::GBRP12LE => 139,
            PixelFormat::GBRP14BE => 140,
            PixelFormat::GBRP14LE => 141,
        }
    }

    #[must_use]
    pub fn is_yuv(&self) -> bool {
        (self.flags() & PIX_FMT_FLAG_RGB == 0) && self.comps().len() >= 2
        //return !(desc->flags & AV_PIX_FMT_FLAG_RGB) && desc->nb_components >= 2;
    }

    #[must_use]
    pub fn is_planar_yuv(&self) -> bool {
        (self.flags() & PIX_FMT_FLAG_PLANAR) != 0 && self.is_yuv()
        //return ((desc->flags & AV_PIX_FMT_FLAG_PLANAR) && isYUV(pix_fmt));
    }

    #[must_use]
    pub fn is_packed(&self) -> bool {
        self.comps().len() >= 2 && (self.flags() & PIX_FMT_FLAG_PLANAR) == 0
        /*return (desc->nb_components >= 2 &&
         !(desc->flags & AV_PIX_FMT_FLAG_PLANAR)) ||
        pix_fmt == AV_PIX_FMT_PAL8 || pix_fmt == AV_PIX_FMT_MONOBLACK ||
        pix_fmt == AV_PIX_FMT_MONOWHITE;*/
    }
}

impl std::fmt::Display for PixelFormat {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.name())
    }
}

#[derive(Debug, Error)]
pub enum TryFromPixelFormatError {
    #[error("unsupported pixel format: '{0}")]
    Unsupported(i32),
}

impl TryFrom<i32> for PixelFormat {
    type Error = TryFromPixelFormatError;

    fn try_from(value: i32) -> Result<Self, Self::Error> {
        match value {
            0 => Ok(PixelFormat::YUV420P),
            2 => Ok(PixelFormat::RGB24),
            4 => Ok(PixelFormat::YUV422P),
            5 => Ok(PixelFormat::YUV444P),
            8 => Ok(PixelFormat::GRAY8),
            12 => Ok(PixelFormat::YUVJ420P),
            13 => Ok(PixelFormat::YUVJ422P),
            14 => Ok(PixelFormat::YUVJ444P),
            33 => Ok(PixelFormat::YUVA420P),
            61 => Ok(PixelFormat::YUV420P9BE),
            62 => Ok(PixelFormat::YUV420P9LE),
            63 => Ok(PixelFormat::YUV420P10BE),
            64 => Ok(PixelFormat::YUV420P10LE),
            65 => Ok(PixelFormat::YUV422P10BE),
            66 => Ok(PixelFormat::YUV422P10LE),
            67 => Ok(PixelFormat::YUV444P9BE),
            68 => Ok(PixelFormat::YUV444P9LE),
            69 => Ok(PixelFormat::YUV444P10BE),
            70 => Ok(PixelFormat::YUV444P10LE),
            71 => Ok(PixelFormat::YUV422P9BE),
            72 => Ok(PixelFormat::YUV422P9LE),
            73 => Ok(PixelFormat::GBRP),
            75 => Ok(PixelFormat::GBRP9BE),
            76 => Ok(PixelFormat::GBRP9LE),
            77 => Ok(PixelFormat::GBRP10BE),
            78 => Ok(PixelFormat::GBRP10LE),
            126 => Ok(PixelFormat::YUV420P12BE),
            127 => Ok(PixelFormat::YUV420P12LE),
            128 => Ok(PixelFormat::YUV420P14BE),
            129 => Ok(PixelFormat::YUV420P14LE),
            130 => Ok(PixelFormat::YUV422P12BE),
            131 => Ok(PixelFormat::YUV422P12LE),
            132 => Ok(PixelFormat::YUV422P14BE),
            133 => Ok(PixelFormat::YUV422P14LE),
            134 => Ok(PixelFormat::YUV444P12BE),
            135 => Ok(PixelFormat::YUV444P12LE),
            136 => Ok(PixelFormat::YUV444P14BE),
            137 => Ok(PixelFormat::YUV444P14LE),
            138 => Ok(PixelFormat::GBRP12BE),
            139 => Ok(PixelFormat::GBRP12LE),
            140 => Ok(PixelFormat::GBRP14BE),
            141 => Ok(PixelFormat::GBRP14LE),
            _ => Err(TryFromPixelFormatError::Unsupported(value)),
        }
    }
}

pub struct ComponentDescriptor {
    // Which of the 4 planes contains the component.
    plane: u8,

    // Number of elements between 2 horizontally consecutive pixels.
    // Elements are bits for bitstream formats, bytes otherwise.
    step: u8,

    // Number of elements before the component of the first pixel.
    // Elements are bits for bitstream formats, bytes otherwise.
    offset: u8,

    // Number of least significant bits that must be shifted away
    // to get the value.
    shift: u8,

    // Number of bits in the component.
    depth: u8,
}

impl ComponentDescriptor {
    #[must_use]
    pub fn plane(&self) -> u8 {
        self.plane
    }
    #[must_use]
    pub fn step(&self) -> u8 {
        self.step
    }
    #[must_use]
    pub fn offset(&self) -> u8 {
        self.offset
    }
    #[must_use]
    pub fn shift(&self) -> u8 {
        self.shift
    }
    #[must_use]
    pub fn depth(&self) -> u8 {
        self.depth
    }
}

// Pixel format is big-endian.
pub const PIX_FMT_FLAG_BE: u64 = 1 << 0;

// Pixel format has a palette in data[1], values are indexes in this palette.
pub const PIX_FMT_FLAG_PAL: u64 = 1 << 1;

// All values of a component are bit-wise packed end to end.
pub const PIX_FMT_FLAG_BITSTREAM: u64 = 1 << 2;

// Pixel format is an HW accelerated format.
pub const PIX_FMT_FLAG_HWACCEL: u64 = 1 << 3;

// At least one pixel component is not in the first data plane.
pub const PIX_FMT_FLAG_PLANAR: u64 = 1 << 4;

// The pixel format contains RGB-like data (as opposed to YUV/grayscale).
pub const PIX_FMT_FLAG_RGB: u64 = 1 << 5;

// The pixel format has an alpha channel. This is set on all formats that
// support alpha in some way, including PIX_FMT_PAL8. The alpha is always
// straight, never pre-multiplied.
//
// If a codec or a filter does not support alpha, it should set all alpha to
// opaque, or use the equivalent pixel formats without alpha component, e.g.
// PIX_FMT_RGB0 (or AV_PIX_FMT_RGB24 etc.) instead of AV_PIX_FMT_RGBA.
pub const PIX_FMT_FLAG_ALPHA: u64 = 1 << 7;

// The pixel format is following a Bayer pattern
//const PIX_FMT_FLAG_BAYER: u64 = 1 << 8;

// The pixel format contains IEEE-754 floating point values. Precision (double,
// single, or half) should be determined by the pixel size (64, 32, or 16 bits).
//const PIX_FMT_FLAG_FLOAT: u64 = 1 << 9;

#[must_use]
pub fn frame_raw_height(pix_fmt: PixelFormat, height: NonZeroU16, plane: u8) -> u16 {
    let shift = if plane == 1 || plane == 2 {
        pix_fmt.log2_chroma_h()
    } else {
        0
    };
    (height.get() + (1 << shift) - 1) >> shift
}

#[allow(clippy::unwrap_used)]
#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;
    use test_case::test_case;

    #[test_case(PixelFormat::YUV420P)]
    #[test_case(PixelFormat::RGB24)]
    #[test_case(PixelFormat::YUV422P)]
    #[test_case(PixelFormat::YUV444P)]
    #[test_case(PixelFormat::GRAY8)]
    #[test_case(PixelFormat::YUVJ420P)]
    #[test_case(PixelFormat::YUVJ422P)]
    #[test_case(PixelFormat::YUVJ444P)]
    #[test_case(PixelFormat::YUVA420P)]
    #[test_case(PixelFormat::YUV420P9BE)]
    #[test_case(PixelFormat::YUV420P9LE)]
    #[test_case(PixelFormat::YUV420P10BE)]
    #[test_case(PixelFormat::YUV420P10LE)]
    #[test_case(PixelFormat::YUV422P10BE)]
    #[test_case(PixelFormat::YUV422P10LE)]
    #[test_case(PixelFormat::YUV444P9BE)]
    #[test_case(PixelFormat::YUV444P9LE)]
    #[test_case(PixelFormat::YUV444P10BE)]
    #[test_case(PixelFormat::YUV444P10LE)]
    #[test_case(PixelFormat::YUV422P9BE)]
    #[test_case(PixelFormat::YUV422P9LE)]
    #[test_case(PixelFormat::GBRP)]
    #[test_case(PixelFormat::GBRP9BE)]
    #[test_case(PixelFormat::GBRP9LE)]
    #[test_case(PixelFormat::GBRP10BE)]
    #[test_case(PixelFormat::GBRP10LE)]
    #[test_case(PixelFormat::YUV420P12BE)]
    #[test_case(PixelFormat::YUV420P12LE)]
    #[test_case(PixelFormat::YUV420P14BE)]
    #[test_case(PixelFormat::YUV420P14LE)]
    #[test_case(PixelFormat::YUV422P12BE)]
    #[test_case(PixelFormat::YUV422P12LE)]
    #[test_case(PixelFormat::YUV422P14BE)]
    #[test_case(PixelFormat::YUV422P14LE)]
    #[test_case(PixelFormat::YUV444P12BE)]
    #[test_case(PixelFormat::YUV444P12LE)]
    #[test_case(PixelFormat::YUV444P14BE)]
    #[test_case(PixelFormat::YUV444P14LE)]
    #[test_case(PixelFormat::GBRP12BE)]
    #[test_case(PixelFormat::GBRP12LE)]
    #[test_case(PixelFormat::GBRP14BE)]
    #[test_case(PixelFormat::GBRP14LE)]
    fn test_pixel_format(pix_fmt: PixelFormat) {
        assert_eq!(pix_fmt, PixelFormat::try_from(pix_fmt.as_i32()).unwrap());
    }
}
