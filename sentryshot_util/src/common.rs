/* Fast a/(1<<b) rounded toward +inf. Assume a>=0 and b>=0 */
/*#[macro_export]
macro_rules! ceil_rshift {
    ($a:expr, $b:expr) => {
        //$a / ($a << $b)
        $a >> $b
        //-((-($a)) >> ($b))
    };
}*/
//#define AV_CEIL_RSHIFT(a,b) (!av_builtin_constant_p(b) ? -((-(a)) >> (b)) : ((a) + (1<<(b)) - 1) >> (b))
//fn ceil_rshift(a: usize, b: u8) -> usize {
//    a / (a << b)
#[must_use]
#[allow(
    clippy::cast_sign_loss,
    clippy::cast_possible_wrap,
    clippy::as_conversions
)]
pub fn ceil_rshift(a: u16, b: u8) -> u16 {
    //a >> b
    -((-(a as i16)) >> (b)) as u16
}
