use std::ops::Deref;

/// Required number of additionally allocated bytes at the end of the input
/// bitstream for decoding. This is mainly needed because some optimized
/// bitstream readers read 32 or 64 bit at once and could read over the end.<br>
//
// Note: If the first 23 bits of the additional bytes are not 0, then damaged
// MPEG bitstreams could cause overread and segfault.
pub const PADDING_SIZE: usize = 64;

/// `Vec<u8>` but the capacity is padded for SIMD.
#[derive(Clone, Debug, Default)]
pub struct PaddedBytes(Vec<u8>);

impl PaddedBytes {
    #[must_use]
    pub fn new(mut bytes: Vec<u8>) -> Self {
        bytes.reserve(PADDING_SIZE);
        Self(bytes)
    }

    #[must_use]
    pub fn to_vec(self) -> Vec<u8> {
        self.0
    }
}

impl Deref for PaddedBytes {
    type Target = [u8];

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
