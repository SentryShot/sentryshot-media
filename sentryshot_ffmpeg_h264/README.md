# ffmpeg_h264

**State: experimental**

A small rust wrapper around ffmpeg's h264 decoder.

<br>

## License

This library is licensed under [MPL-2.0](https://www.mozilla.org/en-US/MPL/2.0)

[What is the Mozilla Public License?](https://www.mozilla.org/en-US/MPL/2.0/FAQ)