# Scale

**State: experimental**

Pure Rust bicubic image scaler.

Supported pixel formats:

- [x] yuv420p
- [x] yuvj420p

<br>

## License

This library is licensed under [MPL-2.0](https://www.mozilla.org/en-US/MPL/2.0)

[What is the Mozilla Public License?](https://www.mozilla.org/en-US/MPL/2.0/FAQ)