// SPDX-License-Identifier: MPL-2.0+

use crate::{
    ceil_rshift,
    filter::{FilterContext, Instance, SwsFilterDescriptor},
    Scaler,
};
use std::rc::Rc;

// desc0
pub(crate) fn init_desc_hscale(
    filter: Rc<Vec<i16>>,
    filter_pos: Rc<Vec<i32>>,
    filter_size: usize,
    //x_inc: isize,
) -> SwsFilterDescriptor {
    let li = FilterContext {
        filter,
        filter_pos,
        filter_size,
        //x_inc,
    };
    SwsFilterDescriptor {
        instance: Instance::FilterContext(li),
        process: lum_h_scale,
    }
}

fn lum_h_scale(
    c: &mut Scaler,
    desc_index: usize,
    slice_y: isize,
    slice_h: isize,
    src2: &[Vec<u8>],
    _dst2: &mut [Vec<u8>],
) {
    for i in 0..slice_h {
        let src_pos = slice_y + i - c.get_src_slice(desc_index).plane[0].slice_y;
        let dst = c.get_dst_slice_mut(desc_index);
        let dst_w = usize::from(dst.width.get());
        let dst_pos = slice_y + i - dst.plane[0].slice_y;

        let src_slice_index = Scaler::get_src_index(desc_index);
        let dst_slice_index = Scaler::get_dst_index(desc_index);

        let (src, dst) = c.slices.get_lines_mut(
            src_slice_index,
            0,
            usize::try_from(src_pos).expect("hope src_pos fit usize"),
            dst_slice_index,
            0,
            usize::try_from(dst_pos).expect("hope dst_pos fit usize"),
            src2,
        );

        let instance = &c.desc[desc_index].instance.filter();

        (c.hy_scale)(
            dst,
            dst_w,
            src,
            &instance.filter,
            &instance.filter_pos,
            instance.filter_size,
        );

        let dst = c.get_dst_slice_mut(desc_index);
        dst.plane[0].slice_h += 1;
    }
}

// desc1
pub(crate) fn init_desc_chscale(
    filter: Rc<Vec<i16>>,
    filter_pos: Rc<Vec<i32>>,
    filter_size: usize,
    //x_inc: isize,
) -> SwsFilterDescriptor {
    let li = FilterContext {
        filter,
        filter_pos,
        filter_size,
        //x_inc,
    };
    SwsFilterDescriptor {
        instance: Instance::FilterContext(li),
        process: chr_h_scale,
    }
}

fn chr_h_scale(
    c: &mut Scaler,
    desc_index: usize,
    slice_y: isize,
    slice_h: isize,
    src2: &[Vec<u8>],
    _dst2: &mut [Vec<u8>],
) {
    let src_slice_i = Scaler::get_src_index(desc_index);
    let src = c.slices.get_slice(src_slice_i);

    let dst_slice_i = Scaler::get_dst_index(desc_index);
    let dst = c.slices.get_slice(dst_slice_i);

    let dst_w = usize::from(ceil_rshift(dst.width.get(), dst.h_chr_sub_sample));

    let src_pos1 = slice_y - src.plane[1].slice_y;
    let dst_pos1 = slice_y - dst.plane[1].slice_y;

    let src_pos2 = slice_y - src.plane[2].slice_y;
    let dst_pos2 = slice_y - dst.plane[2].slice_y;

    for i in 0..slice_h {
        let src_slice_index = Scaler::get_src_index(desc_index);
        let dst_slice_index = Scaler::get_dst_index(desc_index);
        let (src, dst) = c.slices.get_lines_mut(
            src_slice_index,
            1,
            usize::try_from(src_pos1 + i).expect("hope src_pos fit usize"),
            dst_slice_index,
            1,
            usize::try_from(dst_pos1 + i).expect("hope dst_pos fit usize"),
            src2,
        );

        let instance = c.desc[desc_index].instance.filter();

        (c.hc_scale)(
            dst,
            dst_w,
            src,
            &instance.filter,
            &instance.filter_pos,
            instance.filter_size,
        );

        let (src, dst) = c.slices.get_lines_mut(
            src_slice_index,
            2,
            usize::try_from(src_pos2 + i).expect("hope src_pos fit usize"),
            dst_slice_index,
            2,
            usize::try_from(dst_pos2 + i).expect("hope dst_pos fit usize"),
            src2,
        );

        (c.hc_scale)(
            dst,
            dst_w,
            src,
            &instance.filter,
            &instance.filter_pos,
            instance.filter_size,
        );

        c.get_dst_slice_mut(desc_index).plane[1].slice_h += 1;
        c.get_dst_slice_mut(desc_index).plane[2].slice_h += 1;
    }
    //return sliceH;
}
