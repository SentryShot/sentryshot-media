// SPDX-License-Identifier: MPL-2.0+

#![allow(clippy::unwrap_used)]

use crate::{CreateScalerError, Scaler};
use sentryshot_util::{pixfmt::PixelFormat, Frame};
use std::num::NonZeroU16;
use test_case::test_case;

// ffmpeg -cpuflags 0 -f rawvideo -s 300x300 -pix_fmt yuv420p -i ./input.bin -f rawvideo -s 64x64 ./output.bin
const YUV420P_64X64: &[u8] = include_bytes!("../../testdata/yuv420p_64x64.bin");

#[test_case(
    PixelFormat::YUV420P,
    64, 64, 40, 50,
    YUV420P_64X64,
    include_bytes!("../../testdata/yuv420p_64x64/yuv420p_40x50.bin");
    "downscale x"
)]
#[test_case(
    PixelFormat::YUV420P,
    64, 64, 50, 40,
    YUV420P_64X64,
    include_bytes!("../../testdata/yuv420p_64x64/yuv420p_50x40.bin");
    "downscale y"
)]
#[test_case(
    PixelFormat::YUV420P,
    64, 64, 70, 80,
    YUV420P_64X64,
    include_bytes!("../../testdata/yuv420p_64x64/yuv420p_70x80.bin");
    "upscale y"
)]
#[test_case(
    PixelFormat::YUV420P,
    64, 64, 80, 70,
    YUV420P_64X64,
    include_bytes!("../../testdata/yuv420p_64x64/yuv420p_80x70.bin");
    "upscale x"
)]
#[test_case(
    PixelFormat::YUV420P,
    7, 7, 1, 1,
    &[0].repeat(7 * 7 * 2),
    &[0, 0, 0];
    "rshift"
)]
#[test_case(
    PixelFormat::YUV420P,
    1, 2, 3, 3,
    &[0, 0, 0, 0],
    &[0].repeat(17);
    "get_min_buffer_size"
)]
fn test_scaler(
    pix_fmt: PixelFormat,
    src_width: u16,
    src_height: u16,
    dst_width: u16,
    dst_height: u16,
    input: &[u8],
    want: &[u8],
) {
    let src_width = NonZeroU16::new(src_width).unwrap();
    let src_height = NonZeroU16::new(src_height).unwrap();
    let dst_width = NonZeroU16::new(dst_width).unwrap();
    let dst_height = NonZeroU16::new(dst_height).unwrap();

    let mut scaler = Scaler::new(src_width, src_height, pix_fmt, dst_width, dst_height).unwrap();

    let src_frame = Frame::from_raw(input, pix_fmt, src_width, src_height, 1).unwrap();
    let mut dst_frame = Frame::new();
    scaler.scale(&src_frame, &mut dst_frame).unwrap();

    let mut buf = Vec::new();
    dst_frame.copy_to_buffer(&mut buf, 1).unwrap();

    assert_eq!(buf, want);
    scaler.scale(&src_frame, &mut dst_frame).unwrap();

    dst_frame.copy_to_buffer(&mut buf, 1).unwrap();
    assert_eq!(buf, want);

    assert_eq!(dst_width, dst_frame.width());
    assert_eq!(dst_height, dst_frame.height());
    assert_eq!(pix_fmt, dst_frame.pix_fmt());
}

#[test]
fn test_scaler_cascaded() {
    assert!(matches!(
        Scaler::new(
            NonZeroU16::new(5).unwrap(),
            NonZeroU16::new(258).unwrap(),
            PixelFormat::YUV420P,
            NonZeroU16::new(3).unwrap(),
            NonZeroU16::new(3).unwrap(),
        ),
        Err(CreateScalerError::CascadedUnsupported)
    ));
}

/*
#[test]
fn test_scaler_fuzz() {
    for w1 in 3..100 {
        for h1 in 3..100 {
            for w2 in 3..100 {
                println!("{w1} {h1} {w2}");
                for h2 in 3..100 {
                    let src_width = w1;
                    let src_height = h1;
                    let pix_fmt = PixelFormat::YUV420P;
                    let dst_width = w2;
                    let dst_height = h2;

                    let result = Scaler::new(src_width, src_height, pix_fmt, dst_width, dst_height);
                    let mut scaler = match result {
                        Ok(v) => v,
                        Err(ScalerError::CascadedUnsupported) => {
                            println!("cascaded");
                            continue;
                        }
                        Err(e) => panic!("{}", e),
                    };

                    let buf = vec![0; usize::from(src_width) * usize::from(src_height) * 3];
                    let src_frame =
                        Frame::from_raw(&buf, pix_fmt, src_width, src_height, 1).unwrap();

                    let mut dst_frame = Frame::new();
                    scaler.scale(&src_frame, &mut dst_frame).unwrap();
                    scaler.scale(&src_frame, &mut dst_frame).unwrap();
                }
            }
        }
    }
}*/
