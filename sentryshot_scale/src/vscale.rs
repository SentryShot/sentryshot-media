// SPDX-License-Identifier: MPL-2.0+

use crate::{
    ceil_rshift,
    filter::{Instance, SwsFilterDescriptor, VScalerContext},
    scale::Yuv2planarXFn,
    Filters, Scaler,
};
use std::{num::NonZeroU16, rc::Rc};

pub(crate) fn init_vscale_pfn(
    filters: &Filters,
    yuv2plane_x: Yuv2planarXFn,
) -> (VScalerContext, VScalerContext) {
    let chr_ctx = VScalerContext {
        filter: [filters.v_chr_filter.clone(), Rc::new(Vec::new())],
        filter_pos: filters.v_chr_filter_pos.clone(),
        filter_size: filters.v_chr_filter_size,
        yuv2planar_x: Some(yuv2plane_x),
    };
    let lum_ctx = VScalerContext {
        filter: [filters.v_lum_filter.clone(), filters.v_lum_filter.clone()],
        filter_pos: filters.v_lum_filter_pos.clone(),
        filter_size: filters.v_lum_filter_size,
        yuv2planar_x: Some(yuv2plane_x),
    };

    (chr_ctx, lum_ctx)
}

pub(crate) fn init_vscale(
    filters: &Filters,
    yuv2plane_x: Yuv2planarXFn,
) -> (SwsFilterDescriptor, SwsFilterDescriptor) {
    let (chr_ctx, lum_ctx) = init_vscale_pfn(filters, yuv2plane_x);

    let desc2 = SwsFilterDescriptor {
        instance: Instance::VScalerContext(lum_ctx),
        process: lum_planar_vscale,
    };
    let desc3 = SwsFilterDescriptor {
        instance: Instance::VScalerContext(chr_ctx),
        process: chr_planar_vscale,
    };

    (desc2, desc3)
}

const SWS_PB_64: [u8; 8] = [64, 64, 64, 64, 64, 64, 64, 64];

fn lum_planar_vscale(
    c: &mut Scaler,
    desc_index: usize,
    slice_y: isize,
    _slice_h: isize,
    _src2: &[Vec<u8>],
    dst2: &mut [Vec<u8>],
) {
    let instance = &c.desc[desc_index].instance.v_scaler();
    let filter_size = instance.filter_size;
    let filter_size2 = i32::try_from(filter_size).expect("hope filter_size fit i32");

    let slice_y2 = usize::try_from(slice_y).expect("hope slice_y fit usize");
    let first = (1 - filter_size2).max(instance.filter_pos[slice_y2]);

    let src = c.get_src_slice(desc_index).to_owned();
    let sp = first - i32::try_from(src.plane[0].slice_y).expect("hope slice_y fit i32");

    let dst = &c.get_dst_slice(desc_index);
    let dst_w = dst.width;
    let dp = slice_y - dst.plane[0].slice_y;

    let instance = &c.desc[desc_index].instance.v_scaler();
    let filter = instance.filter[0][slice_y2 * filter_size..].to_owned();

    if filter_size2 != 1 {
        (instance.yuv2planar_x.expect("hope yuv2planar exists"))(
            c,
            &filter,
            instance.filter_size,
            desc_index,
            0,
            usize::try_from(sp).expect("hope sp fit usize"),
            0,
            usize::try_from(dp).expect("hope dp fit usize"),
            dst_w,
            &SWS_PB_64,
            0,
            dst2,
        );
    }
}

#[allow(clippy::similar_names)]
fn chr_planar_vscale(
    c: &mut Scaler,
    desc_i: usize,
    slice_y: isize,
    _slice_h: isize,
    _src2: &[Vec<u8>],
    dst2: &mut [Vec<u8>],
) {
    let dst = c.get_dst_slice_mut(desc_i);
    let chr_skip_mask: isize = (1 << dst.v_chr_sub_sample) - 1;
    if slice_y & chr_skip_mask != 0 {
        return;
    }

    let dst = c.get_dst_slice_mut(desc_i);
    let dst_w = NonZeroU16::new(ceil_rshift(dst.width.get(), dst.h_chr_sub_sample))
        .expect("hope this isn't zero");
    let chr_slice_y = slice_y >> dst.v_chr_sub_sample;

    let instance = c.desc[desc_i].instance.v_scaler();
    let first = (1 - i32::try_from(instance.filter_size).expect("hope filter_size fit i32"))
        .max(instance.filter_pos[usize::try_from(chr_slice_y).expect("hope slice_y fit usize")]);
    let first2 = isize::try_from(first).expect("hope first fit isize");
    let src = c.get_src_slice(desc_i).to_owned();
    let sp1: isize = first2 - src.plane[1].slice_y;
    let sp2: isize = first2 - src.plane[2].slice_y;

    let dst = c.get_dst_slice_mut(desc_i);
    let dp1: isize = chr_slice_y - dst.plane[1].slice_y;
    let dp2: isize = chr_slice_y - dst.plane[2].slice_y;

    let instance = c.desc[desc_i].instance.v_scaler();
    let filter = instance.filter[0]
        [usize::try_from(chr_slice_y).expect("hope slice_y fit usize") * instance.filter_size..]
        .to_owned();
    let filter_size = instance.filter_size;

    if filter_size != 1 {
        (instance.yuv2planar_x.expect("hope yuv2planar_x exist"))(
            c,
            &filter,
            filter_size,
            desc_i,
            1,
            usize::try_from(sp1).expect("hope sp1 fit usize"),
            1,
            usize::try_from(dp1).expect("hope dp1 fit usize"),
            dst_w,
            &SWS_PB_64,
            0,
            dst2,
        );
        let instance = c.desc[desc_i].instance.v_scaler();
        (instance.yuv2planar_x.expect("hope yuv2planar_x exist"))(
            c,
            &filter,
            filter_size,
            desc_i,
            2,
            usize::try_from(sp2).expect("hope sp2 fit usize"),
            2,
            usize::try_from(dp2).expect("hope dp2 fit usize"),
            dst_w,
            &SWS_PB_64,
            3,
            dst2,
        );
    }
}
