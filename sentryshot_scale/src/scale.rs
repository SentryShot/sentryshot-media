// SPDX-License-Identifier: MPL-2.0+

use crate::{HScaleFn, Scaler};
use sentryshot_util::clip_u8;
use std::num::NonZeroU16;

// bilinear / bicubic scaling
#[allow(clippy::too_many_arguments)]
fn h_scale8_to15_c(
    dst: &mut [u8],
    dst_w: usize,
    src: &[u8],
    filter: &[i16],
    filter_pos: &[i32],
    filter_size: usize,
) {
    for i in 0..dst_w {
        let src_posx = filter_pos[i];

        let mut val = 0;
        for j in 0..filter_size {
            let src_posx = usize::try_from(src_posx).expect("hope posx fit usize");
            val += isize::from(src[src_posx + j]) * isize::from(filter[filter_size * i + j]);
            // val += ((int)src[srcPos + j]) *
            //         (int16_t)instance->filter[instance->filter_size * i + j];
        }

        // the cubic equation does overflow ...
        let (a, b) = i16tou8(
            i16::try_from(isize::min(val >> 7, (1 << 15) - 1))
                .expect("hope cubic equation fit i16"),
        );
        dst[(i * 2) + 1] = a;
        dst[i * 2] = b;
    }
}

#[allow(clippy::cast_sign_loss, clippy::as_conversions)]
fn i16tou8(v: i16) -> (u8, u8) {
    ((v >> 8) as u8, (v & 0xff) as u8)
}

#[allow(clippy::similar_names)]
pub(crate) fn init_scale(src_bpc: u8) -> (Yuv2planarXFn, HScaleFn, HScaleFn) {
    let hy_scale: HScaleFn;
    let hc_scale: HScaleFn;
    if src_bpc == 8 {
        if src_bpc <= 14 {
            hy_scale = h_scale8_to15_c;
            hc_scale = h_scale8_to15_c;
        } else {
            todo!();
        }
    } else {
        todo!();
    }
    /*if (c->srcBpc == 8) {
      if (c->srcBpc <= 14) {
        c->hyScale = c->hcScale = hScale8To15_c;
      } else {
        c->hyScale = c->hcScale = hScale8To19_c;
      }
    } else {
      c->hyScale = c->hcScale = c->srcBpc > 14 ? hScale16To19_c : hScale16To15_c;
    }*/

    (init_output_funcs(), hy_scale, hc_scale)
}

//static void yuv2planeX_8_c(SwsContext *c, const int16_t *filter, int filterSize,
//int desc_index, int src_index, int src_offset,
//int dst_index, int dst_offset, int dstW,
//const uint8_t *dither, int offset,
//uint8_t *dst2[4])

#[allow(clippy::too_many_arguments)]
fn yuv2plane_x_8_c(
    scaler: &mut Scaler,
    filter: &[i16],
    filter_size: usize,
    desc_i: usize,
    src_plane_i: u8,
    src_line_i: usize,
    dst_plane_i: u8,
    dst_line_i: usize,
    dst_w: NonZeroU16,
    dither: &[u8],
    offset: usize,
    dst2: &mut [Vec<u8>],
) {
    #[allow(clippy::needless_range_loop)]
    for i in 0..usize::from(dst_w.get()) {
        let mut val = i32::from(dither[(i + offset) & 7]) << 12;
        for j in 0..filter_size {
            let slice_i = Scaler::get_src_index(desc_i);
            let src = scaler.slices.get_line(slice_i, src_plane_i, src_line_i + j);
            //uint8_t *line = get_line3(src, src_index, src_offset + j);
            let v = u8toi16(src[(i * 2) + 1], src[i * 2]);
            val += i32::from(v) * i32::from(filter[j]);
            //val += x * filter[j];
        }

        let slice_i = Scaler::get_dst_index(desc_i);
        let dst = scaler
            .slices
            .get_line_mut(slice_i, dst_plane_i, dst_line_i, dst2);
        dst[i] = clip_u8(val >> 19);
        //get_line3(dst, dst_index, dst_offset)[i] = av_clip_uint8(val >> 19);
    }
}

fn u8toi16(a: u8, b: u8) -> i16 {
    (i16::from(a) << 8) | i16::from(b)
}

pub(crate) fn init_output_funcs() -> Yuv2planarXFn {
    yuv2plane_x_8_c
}

// Write one line of horizontally scaled data to planar output
// with multi-point vertical scaling between input pixels.
//
// @param filter        vertical luma/alpha scaling coefficients, 12 bits [0,4096]
// @param src           scaled luma (Y) or alpha (A) source data, 15 bits for
//                      8-10-bit output, 19 bits for 16-bit output (in int32_t)
// @param filterSize    number of vertical input lines to scale
// @param dest          pointer to output plane. For >8-bit
//                      output, this is in uint16_t
// @param dstW          width of destination pixels
// @param offset        Dither offset
pub(crate) type Yuv2planarXFn = fn(
    scaler: &mut Scaler,
    filter: &[i16],
    filter_size: usize,
    desc_index: usize,
    src_index: u8,
    src_offset: usize,
    dst_index: u8,
    dst_offset: usize,
    dst_w: NonZeroU16,
    dither: &[u8],
    offset: usize,
    dst2: &mut [Vec<u8>],
);
