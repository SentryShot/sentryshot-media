// SPDX-License-Identifier: MPL-2.0+

use std::num::NonZeroU16;

// Slice plane
#[derive(Clone, Default)]
pub(crate) struct SwsPlane {
    // max number of lines that can be hold by this plane
    pub available_lines: u16,

    // Index of first line.
    pub slice_y: isize,

    // Number of lines.
    pub slice_h: isize,

    // Line buffer.
    //uint8_t **line;
    pub lines: Vec<Vec<u8>>,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub(crate) enum Mapping {
    Lines,
    Src,
    Dst,
}

const MAX_SLICE_PLANES: usize = 4;

// Struct which defines a slice of an image to be scaled or an output for a scaled slice.
// A slice can also be used as intermediate ring buffer for scaling steps.
pub(crate) struct SwsSlice {
    // Slice line width
    pub width: NonZeroU16,

    // horizontal chroma subsampling factor
    pub h_chr_sub_sample: u8,
    // vertical chroma subsampling factor
    pub v_chr_sub_sample: u8,

    // flag to identify if there are dynamic allocated lines
    should_free_lines: bool,

    // color planes
    pub plane: [SwsPlane; MAX_SLICE_PLANES], //SwsPlane plane[MAX_SLICE_PLANES];

    pub line_map: [Vec<u16>; 4],
    pub offsets: [Vec<usize>; 4],
    pub plane_map: [Vec<u8>; 4],
    pub mapping: [Vec<Mapping>; 4],
}

#[allow(clippy::too_many_arguments)]
pub(crate) fn init_slice_from_src(
    s: &mut SwsSlice,
    is_src: bool,
    stride: &[usize; 8],
    src_w: NonZeroU16,
    lum_h: u16,
    chr_h: u16,
) {
    let end: [u16; 4] = [lum_h, chr_h, chr_h, lum_h];

    s.width = src_w;

    for i in 0..4_u8 {
        let i2 = usize::from(i);
        let first = s.plane[i2].slice_y;
        let n = s.plane[i2].available_lines;
        let mut lines = end[i2];
        let tot_lines = isize::try_from(end[i2]).expect("hope end fit isize") - first;

        let n2 = isize::try_from(n).expect("hope n fit isize");
        if 0 >= first && n2 >= tot_lines {
            //if (start[i] >= first && n >= tot_lines) {
            s.plane[i2].slice_h = tot_lines.max(s.plane[i2].slice_h);
            for j in 0..lines {
                let line_i = usize::from(0 - u16::try_from(first).expect("hope first fit u16") + j);
                s.line_map[i2][line_i] = 0;
                s.offsets[i2][line_i] = usize::from(j) * stride[i2];
                s.plane_map[i2][line_i] = i;
                if is_src {
                    s.mapping[i2][line_i] = Mapping::Src;
                } else {
                    s.mapping[i2][line_i] = Mapping::Dst;
                }
            }
        } else {
            s.plane[i2].slice_y = 0;
            lines = if lines > n { n } else { lines };
            s.plane[i2].slice_h = isize::try_from(lines).expect("hope lines fit isize");
            for j in 0..usize::from(lines) {
                let line_i = 0 - usize::try_from(first).expect("hope first fit usize") + j;
                s.line_map[i2][line_i] = 0;
                s.offsets[i2][j] = j * stride[i2];
                s.plane_map[i2][line_i] = i;
                if is_src {
                    s.mapping[i2][line_i] = Mapping::Src;
                } else {
                    s.mapping[i2][line_i] = Mapping::Dst;
                }
            }
        }
    }
}

pub(crate) const MAX_LINES_AHEAD: u8 = 4;

// Calculates the minimum ring buffer size, it should be able to store vFilterSize
// more n lines where n is the max difference between each adjacent slice which
// outputs a line.
// The n lines are needed only when there is not enough src lines to output a
// single dst line, then we should buffer these lines to process them on the next
// call to scale.
#[allow(clippy::too_many_arguments)]
pub(crate) fn get_min_buffer_size(
    v_lum_filter_pos: &[i32],
    v_lum_filter_size: usize,
    v_chr_filter_pos: &[i32],
    v_chr_filter_size: usize,
    dst_h: NonZeroU16,
    chr_dst_h: isize,
    chr_src_vsub_sample: u8,
    out_lum_size: &mut usize,
    out_chr_size: &mut usize,
) {
    *out_lum_size = v_lum_filter_size;
    *out_chr_size = v_chr_filter_size;

    #[allow(clippy::needless_range_loop)]
    for lum_y in 0..usize::from(dst_h.get()) {
        let chr_y = usize::try_from(
            isize::try_from(lum_y).expect("hope lum_y fit isize") * chr_dst_h
                / isize::try_from(dst_h.get()).expect("hope dst_h fit isize"),
        )
        .expect("gope chr_y fit usize");
        let v_lum_filter_size = i32::try_from(v_lum_filter_size).expect("hope filter_size fit i32");
        let v_chr_filter_size = i32::try_from(v_chr_filter_size).expect("hope filter_size fit i32");
        let mut next_slice = (v_lum_filter_pos[lum_y] + v_lum_filter_size - 1)
            .max((v_chr_filter_pos[chr_y] + v_chr_filter_size - 1) << chr_src_vsub_sample);
        next_slice >>= chr_src_vsub_sample;
        next_slice <<= chr_src_vsub_sample;
        *out_lum_size = (*out_lum_size).max(
            usize::try_from((next_slice - v_lum_filter_pos[lum_y]).max(0))
                .expect("hope lum_size fit usize"),
        );
        *out_chr_size = (*out_chr_size).max(
            usize::try_from((next_slice >> chr_src_vsub_sample) - v_chr_filter_pos[chr_y])
                .expect("hope chr_size fit usize"),
        );
    }
}

// TODO usize
pub(crate) fn alloc_slice(
    lum_lines: u16,
    chr_lines: u16,
    h_sub_sample: u8,
    v_sub_sample: u8,
    is_ring: bool,
) -> SwsSlice {
    let size: [u16; 4] = [lum_lines, chr_lines, chr_lines, lum_lines];

    let mut plane: [SwsPlane; MAX_SLICE_PLANES] = Default::default();
    let mut tmp_line_map: [Vec<u16>; 4] = Default::default();
    let mut tmp_offsets: [Vec<usize>; 4] = Default::default();
    let mut tmp_planes: [Vec<u8>; 4] = Default::default();
    let mut mapping: [Vec<Mapping>; 4] = Default::default();

    #[allow(clippy::needless_range_loop)]
    for i in 0..4 {
        let n = size[i] * if is_ring { 3 } else { 1 };
        let n2 = usize::from(n);

        plane[i].lines = vec![Vec::default(); n2];
        //s->plane[i].tmp = av_calloc(n, sizeof(*s->plane[i].tmp));

        for j in 0..4 {
            tmp_line_map[j] = vec![0; n2];
            tmp_offsets[j] = vec![0; n2];
            tmp_planes[j] = vec![0; n2];
            mapping[j] = vec![Mapping::Lines; n2];
        }
        for j in 0..n {
            let j2 = usize::from(j);
            tmp_line_map[0][j2] = j;
            tmp_line_map[1][j2] = j;
            tmp_line_map[2][j2] = j;
            tmp_line_map[3][j2] = j;

            tmp_planes[1][j2] = 1;
            tmp_planes[2][j2] = 2;
            tmp_planes[3][j2] = 3;
        }

        plane[i].available_lines = size[i];
        plane[i].slice_y = 0;
        plane[i].slice_h = 0;
    }

    SwsSlice {
        width: NonZeroU16::new(1).expect("1 != 0"),
        h_chr_sub_sample: h_sub_sample,
        v_chr_sub_sample: v_sub_sample,
        should_free_lines: false,

        plane,

        line_map: tmp_line_map,
        offsets: tmp_offsets,
        plane_map: tmp_planes,
        mapping,
    }
}

// Slice lines contains extra bytes for vectorial code thus @size
// is the allocated memory size and @width is the number of pixels
pub(crate) fn alloc_lines(slices: &mut Slices, size: usize, width: NonZeroU16) {
    let idx: [u8; 2] = [3, 2];

    let s = &mut slices.slice1;
    s.should_free_lines = true;
    s.width = width;

    #[allow(clippy::needless_range_loop)]
    for i in 0..2_u8 {
        let i2 = usize::from(i);
        // I = PLANE.
        //
        let s = &mut slices.slice1;
        let n = s.plane[i2].available_lines;
        let n2 = usize::from(n);
        let ii = idx[i2];

        assert_eq!(n, s.plane[usize::from(ii)].available_lines);
        for j in 0..n {
            let j2 = usize::from(j);
            // J = LINE.

            // Chroma plane line U and V are expected to be contiguous in memory
            // by mmx vertical scaler code.
            slices.allocate_line(SliceI::I1, i, j, size * 2 + 32);
            //*get_line8(c, 1, i, j) = av_malloc(size * 2 + 32);
            slices.reset_line(SliceI::I1, i, j);

            let s = &mut slices.slice1;
            s.mapping[i2][j2] = Mapping::Lines;
            s.plane[i2].lines[j2] = vec![0; size * 2 + 32];
            // s->plane[i].tmp[j] = av_malloc(size * 2 + 32);

            slices.map_line(SliceI::I1, i, j2 + n2, i, j2, 0);
            slices.map_line(SliceI::I1, ii, j2, i, j2, size + 16);
            slices.map_line(SliceI::I1, ii, j2 + n2, ii, j2, 0);
        }
    }
}

/*
fn fill_ones(s: &SwsSlice, n: isize, bpc: isize) {
    for i in 0..4 {
        let size = s.plane[i].available_lines;
        for _j in 0..size {
            if bpc == 16 {
                let end = (n >> 1) + 1;
                for _k in 0..end {
                    //((int32_t *)(s->plane[i].line[j]))[k] = 1 << 18;
                }
            } else if bpc == 32 {
                let end = (n >> 2) + 1;
                for _k in 0..end {
                    //((int64_t *)(s->plane[i].line[j]))[k] = 1LL << 34;
                }
            } else {
                let end = n + 1;
                for _k in 0..end {
                    //((int16_t *)(s->plane[i].line[j]))[k] = 1 << 14;
                }
            }
        }
    }
    /*for (i = 0; i < 4; ++i) {
      size = s->plane[i].available_lines;
      for (j = 0; j < size; ++j) {
        if (bpc == 16) {
          end = (n >> 1) + 1;
          for (k = 0; k < end; ++k)
            ((int32_t *)(s->plane[i].line[j]))[k] = 1 << 18;
        } else if (bpc == 32) {
          end = (n >> 2) + 1;
          for (k = 0; k < end; ++k)
            ((int64_t *)(s->plane[i].line[j]))[k] = 1LL << 34;
        } else {
          end = n + 1;
          for (k = 0; k < end; ++k)
            ((int16_t *)(s->plane[i].line[j]))[k] = 1 << 14;
        }
      }
    }*/
}*/

pub(crate) fn rotate_slice(s: &mut SwsSlice, lum: isize, chr: isize) {
    if lum != 0 {
        for i in (0..4).step_by(3) {
            let n = isize::try_from(s.plane[i].available_lines)
                .expect("hope available_lines fit isize");
            let l = lum - s.plane[i].slice_y;
            if l >= n * 2 {
                s.plane[i].slice_y += n;
                s.plane[i].slice_h -= n;
            }
        }
    };

    if chr != 0 {
        for i in 1..3 {
            let n = isize::try_from(s.plane[i].available_lines)
                .expect("hope available_lines fit isize");
            let l = chr - s.plane[i].slice_y;
            if l >= n * 2 {
                s.plane[i].slice_y += n;
                s.plane[i].slice_h -= n;
            }
        }
    }
}

#[derive(Clone, Copy)]
pub(crate) enum SliceI {
    I0,
    I1,
    I2,
}

pub(crate) struct Slices {
    pub slice0: SwsSlice,
    pub slice1: SwsSlice,
    pub slice2: SwsSlice,
}

impl Slices {
    pub fn get_slice(&self, slice_index: SliceI) -> &SwsSlice {
        match slice_index {
            SliceI::I0 => &self.slice0,
            SliceI::I1 => &self.slice1,
            SliceI::I2 => &self.slice2,
        }
    }
    pub fn get_slice_mut(&mut self, slice_index: SliceI) -> &mut SwsSlice {
        match slice_index {
            SliceI::I0 => &mut self.slice0,
            SliceI::I1 => &mut self.slice1,
            SliceI::I2 => &mut self.slice2,
        }
    }

    pub fn get_line(&self, slice_index: SliceI, plane_i: u8, line_i: usize) -> &[u8] {
        let s = self.get_slice(slice_index);
        let plane_i = usize::from(plane_i);
        let offset = s.offsets[plane_i][line_i];

        if s.mapping[plane_i][line_i] == Mapping::Src {
            todo!();
            //return &src.unwrap()[plane_i][offset..];
        }
        if s.mapping[plane_i][line_i] == Mapping::Dst {
            todo!();
            //return &dst.unwrap()[plane_i][offset..];
        }

        let line_i = usize::from(s.line_map[plane_i][line_i]);
        let plane_i = usize::from(s.plane_map[plane_i][line_i]);
        assert_eq!(line_i, usize::from(s.line_map[plane_i][line_i]));
        assert_eq!(plane_i, usize::from(s.plane_map[plane_i][line_i]));

        &s.plane[plane_i].lines[line_i][offset..]
    }

    pub fn get_line_mut<'a>(
        &'a mut self,
        slice_index: SliceI,
        plane_i: u8,
        line_i: usize,
        dst: &'a mut [Vec<u8>],
    ) -> &mut [u8] {
        let s = self.get_slice_mut(slice_index);
        let plane_i = usize::from(plane_i);
        let offset = s.offsets[plane_i][line_i];

        assert_ne!(s.mapping[plane_i][line_i], Mapping::Src);
        if s.mapping[plane_i][line_i] == Mapping::Dst {
            return &mut dst[plane_i][offset..];
        }

        let line_i = usize::from(s.line_map[plane_i][line_i]);
        let plane_i = usize::from(s.plane_map[plane_i][line_i]);

        assert_eq!(line_i, usize::from(s.line_map[plane_i][line_i]));
        assert_eq!(plane_i, usize::from(s.plane_map[plane_i][line_i]));

        &mut s.plane[plane_i].lines[line_i][offset..]
    }

    #[allow(clippy::too_many_arguments)]
    pub fn get_lines_mut<'a>(
        &'a mut self,
        src_slice_index: SliceI,
        src_plane_i: u8,
        src_line_i: usize,
        dst_slice_index: SliceI,
        dst_plane_i: u8,
        dst_line_i: usize,
        src: &'a [Vec<u8>],
        //dst: Option<&'a mut [Vec<u8>]>,
    ) -> (&[u8], &mut [u8]) {
        use SliceI::*;
        let (src_slice, dst_slice) = match (src_slice_index, dst_slice_index) {
            (I0, I1) => (&self.slice0, &mut self.slice1),
            (I0, I2) => (&self.slice0, &mut self.slice2),
            (I1, I0) => (&self.slice1, &mut self.slice0),
            (I1, I2) => (&self.slice1, &mut self.slice2),
            (I2, I0) => (&self.slice2, &mut self.slice0),
            (I2, I1) => (&self.slice2, &mut self.slice1),
            _ => panic!("invalid slice combination"),
        };
        (
            {
                let plane_i = usize::from(src_plane_i);
                let offset = src_slice.offsets[plane_i][src_line_i];

                assert_ne!(src_slice.mapping[plane_i][src_line_i], Mapping::Dst);
                if src_slice.mapping[plane_i][src_line_i] == Mapping::Src {
                    &src[plane_i][offset..]
                } else {
                    let line_i = usize::from(src_slice.line_map[plane_i][src_line_i]);
                    let plane_i = usize::from(src_slice.plane_map[plane_i][line_i]);
                    assert_eq!(line_i, usize::from(src_slice.line_map[plane_i][line_i]));
                    assert_eq!(plane_i, usize::from(src_slice.plane_map[plane_i][line_i]));

                    &src_slice.plane[plane_i].lines[line_i][offset..]
                }
            },
            {
                let plane_i = usize::from(dst_plane_i);
                let offset = dst_slice.offsets[plane_i][dst_line_i];

                assert_ne!(dst_slice.mapping[plane_i][dst_line_i], Mapping::Src);
                if dst_slice.mapping[plane_i][dst_line_i] == Mapping::Dst {
                    todo!();
                    //&mut dst.unwrap()[plane_i][offset..]
                } else {
                    let line_i = usize::from(dst_slice.line_map[plane_i][dst_line_i]);
                    let plane_i = usize::from(dst_slice.plane_map[plane_i][line_i]);

                    assert_eq!(line_i, usize::from(dst_slice.line_map[plane_i][line_i]));
                    assert_eq!(plane_i, usize::from(dst_slice.plane_map[plane_i][line_i]));

                    &mut dst_slice.plane[plane_i].lines[line_i][offset..]
                }
            },
        )
    }

    fn allocate_line(&mut self, slice_index: SliceI, plane_i: u8, line_i: u16, size: usize) {
        let plane_i = usize::from(plane_i);
        let s = self.get_slice_mut(slice_index);

        let line_i = usize::from(s.line_map[plane_i][usize::from(line_i)]);
        let plane_i = usize::from(s.plane_map[plane_i][line_i]);

        assert_eq!(line_i, usize::from(s.line_map[plane_i][line_i]));
        assert_eq!(plane_i, usize::from(s.plane_map[plane_i][line_i]));

        s.plane[plane_i].lines[line_i].resize(size, 0);
    }

    fn map_line(
        &mut self,
        slice_index: SliceI,
        a_plane_i: u8,
        a_line_i: usize,
        b_plane_i: u8,
        b_line_i: usize,
        b_offset: usize,
    ) {
        let s = self.get_slice_mut(slice_index);
        assert!(s.mapping[usize::from(b_plane_i)][b_line_i] == Mapping::Lines);

        let b_offset = s.offsets[usize::from(b_plane_i)][b_line_i] + b_offset;
        let b_plane_i = s.plane_map[usize::from(b_plane_i)][b_line_i];
        let b_line_i = s.line_map[usize::from(b_plane_i)][b_line_i];

        assert!(s.mapping[usize::from(b_plane_i)][usize::from(b_line_i)] == Mapping::Lines);

        s.offsets[usize::from(a_plane_i)][a_line_i] = b_offset;
        s.line_map[usize::from(a_plane_i)][a_line_i] = b_line_i;
        s.plane_map[usize::from(a_plane_i)][a_line_i] = b_plane_i;

        assert_eq!(
            b_line_i,
            s.line_map[usize::from(b_plane_i)][usize::from(b_line_i)]
        );
        assert_eq!(
            b_plane_i,
            s.plane_map[usize::from(b_plane_i)][usize::from(b_line_i)]
        );
    }

    fn reset_line(&mut self, slice_index: SliceI, plane_i: u8, line_i: u16) {
        let plane_i2 = usize::from(plane_i);
        let line_i2 = usize::from(line_i);
        let s = self.get_slice_mut(slice_index);

        s.plane_map[plane_i2][line_i2] = plane_i;
        s.offsets[plane_i2][line_i2] = 0;
        s.line_map[plane_i2][line_i2] = line_i;
    }
}
