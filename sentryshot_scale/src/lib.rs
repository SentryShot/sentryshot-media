// SPDX-License-Identifier: MPL-2.0+

mod filter;
mod hscale;
mod scale;
mod slice;
mod vscale;

#[cfg(test)]
mod test;

pub use sentryshot_padded_bytes::PaddedBytes;
pub use sentryshot_util::{pixfmt::PixelFormat, Frame};

use crate::{
    hscale::{init_desc_chscale, init_desc_hscale},
    scale::init_scale,
    slice::{alloc_lines, alloc_slice, get_min_buffer_size, rotate_slice, MAX_LINES_AHEAD},
    vscale::init_vscale,
};
use filter::{init_filter, SwsFilterDescriptor};
use sentryshot_util::{ceil_rshift, ffalign, ResetBufferError};
use slice::{init_slice_from_src, SliceI, Slices, SwsSlice};
use std::{num::NonZeroU16, panic, rc::Rc, sync::Mutex};
use thiserror::Error;

pub struct Scaler {
    src_width: NonZeroU16,
    src_height: NonZeroU16,
    dst_width: NonZeroU16,
    dst_height: NonZeroU16,

    chr_src_vsub_sample: u8,

    num_desc: u8,
    desc_index: [u8; 2],

    slices: Slices,
    desc: [SwsFilterDescriptor; 4],

    //yuv2plane_x: Yuv2planarXFn,
    hy_scale: HScaleFn,
    hc_scale: HScaleFn,

    pix_fmt: PixelFormat,

    filters: Filters,

    // Height of source chroma planes.
    chr_src_h: u16,
}

type HScaleFn = fn(
    dst: &mut [u8],
    dst_w: usize,
    src: &[u8],
    filter: &[i16],
    filter_pos: &[i32],
    filter_size: usize,
);

#[derive(Debug, Error)]
pub enum CreateScalerError {
    #[error("unsupported pixel format: {0}")]
    UnsupportedPixelFormat(PixelFormat),

    // FFmpeg does the scaling in 2 steps for some inputs.
    #[error("cascaded supported")]
    CascadedUnsupported,

    #[error("scaler paniced: {0}")]
    Panic(String),
}

#[derive(Debug, Error)]
pub enum ScalerError {
    #[error("width changed: {0}vs{1}")]
    WidthChanged(NonZeroU16, NonZeroU16),

    #[error("height changed: {0}vs{1}")]
    HeightChanged(NonZeroU16, NonZeroU16),

    #[error("pixel format changed: {0}vs{1}")]
    PixelFormatChanged(PixelFormat, PixelFormat),

    #[error("reset buffer: {0}")]
    ResetBuffer(#[from] ResetBufferError),

    #[error("scaler paniced: {0}")]
    Panic(String),
}

impl Scaler {
    pub fn new(
        src_width: NonZeroU16,
        src_height: NonZeroU16,
        pix_fmt: PixelFormat,
        dst_width: NonZeroU16,
        dst_height: NonZeroU16,
    ) -> Result<Self, CreateScalerError> {
        match pix_fmt {
            PixelFormat::YUV420P | PixelFormat::YUVJ420P => {}
            _ => return Err(CreateScalerError::UnsupportedPixelFormat(pix_fmt)),
        }

        let result = panic::catch_unwind(|| {
            Self::new_internal(src_width, src_height, pix_fmt, dst_width, dst_height)
        });
        match result {
            Ok(v) => Ok(v?),
            Err(_) => Err(CreateScalerError::Panic(format!(
                "src_width={src_width} src_height={src_height} pix_fmt={pix_fmt} dst_width={dst_width} dst_height={dst_height}",
            ))),
        }
    }

    #[allow(
        clippy::too_many_arguments,
        clippy::too_many_lines,
        clippy::similar_names
    )]
    fn new_internal(
        src_width: NonZeroU16,
        src_height: NonZeroU16,
        pix_fmt: PixelFormat,
        dst_width: NonZeroU16,
        dst_height: NonZeroU16,
    ) -> Result<Self, CreateScalerError> {
        // Precalculate horizontal scaler filter coefficients.
        const FILTER_ALIGN: i32 = 1;
        /*const int filterAlign = PPC_ALTIVEC(cpu_flags) ? 8
        : have_neon(cpu_flags) ? 4
        : have_lasx(cpu_flags) ? 8
                               : 1;*/

        let src_width2 = i64::from(src_width.get());
        let dst_width2 = i64::from(dst_width.get());
        let lum_xinc = ((src_width2 << 16) + (dst_width2 >> 1)) / dst_width2;
        //c->lumXInc = (((int64_t)srcW << 16) + (dstW >> 1)) / dstW;

        let (h_lum_filter, h_lum_filter_pos, h_lum_filter_size) = init_filter(
            isize::try_from(lum_xinc).expect("hope xinc fit isize"),
            src_width.get(),
            dst_width.get(),
            FILTER_ALIGN,
            1 << 14,
            get_local_pos(0, 0),
            get_local_pos(0, 0),
        )?;

        /*shuffle_filter_coefficients(
            &mut scaler.h_lum_filter_pos,
            scaler.h_lum_filter_size,
            &mut scaler.h_lum_filter,
            dst_w,
        );*/
        /*if (ff_shuffle_filter_coefficients(c, c->hLumFilterPos, c->hLumFilterSize,
                                         c->hLumFilter, dstW) < 0)
        goto nomem;*/

        let chr_src_hsub_sample = pix_fmt.log2_chroma_w();
        let chr_src_vsub_sample = pix_fmt.log2_chroma_h();

        // Note the AV_CEIL_RSHIFT is so that we always round toward +inf.
        let chr_src_w = ceil_rshift(src_width.get(), chr_src_hsub_sample);
        let chr_src_h = ceil_rshift(src_height.get(), chr_src_vsub_sample);
        let chr_dst_w = ceil_rshift(dst_width.get(), chr_src_hsub_sample);
        let chr_dst_h = ceil_rshift(dst_height.get(), chr_src_vsub_sample);
        assert!(chr_dst_h <= dst_height.get());

        #[allow(clippy::unwrap_used)]
        let chr_xinc = isize::try_from(
            ((i64::from(chr_src_w) << 16) + (i64::from(chr_dst_w) >> 1)) / i64::from(chr_dst_w),
        )
        .unwrap();
        //c->chrXInc = (((int64_t)c->chrSrcW << 16) + (c->chrDstW >> 1)) / c->chrDstW;
        #[allow(clippy::unwrap_used)]
        let chr_yinc = isize::try_from(
            ((i64::from(chr_src_h) << 16) + (i64::from(chr_dst_h) >> 1)) / i64::from(chr_dst_h),
        )
        .unwrap();
        //c->chrYInc = (((int64_t)c->chrSrcH << 16) + (c->chrDstH >> 1)) / c->chrDstH;

        let (h_chr_filter, h_chr_filter_pos, h_chr_filter_size) = init_filter(
            chr_xinc,
            chr_src_w,
            chr_dst_w,
            FILTER_ALIGN,
            1 << 14,
            get_local_pos(chr_src_hsub_sample.into(), -513),
            get_local_pos(chr_src_hsub_sample.into(), -513),
        )?;

        /*shuffle_filter_coefficients(
            &mut scaler.h_chr_filter_pos,
            scaler.h_chr_filter_size,
            &mut scaler.h_chr_filter,
            u16::try_from(scaler.chr_src_w).unwrap(),
        );*/
        /*  if (ff_shuffle_filter_coefficients(c, c->hChrFilterPos, c->hChrFilterSize,
                                         c->hChrFilter, c->chrSrcW) < 0)
        goto nomem;*/

        let src_height2 = i64::from(src_height.get());
        let dst_height2 = i64::from(dst_height.get());
        let lum_yinc = ((src_height2 << 16) + (dst_height2 >> 1)) / dst_height2;
        //c->lumYInc = (((int64_t)srcH << 16) + (dstH >> 1)) / dstH;

        // Precalculate vertical scaler filter coefficients.
        let (v_lum_filter, v_lum_filter_pos, v_lum_filter_size) = init_filter(
            isize::try_from(lum_yinc).expect("hope yinc fit isize"),
            src_height.get(),
            dst_height.get(),
            FILTER_ALIGN,
            1 << 12,
            get_local_pos(0, 0),
            get_local_pos(0, 0),
        )?;

        let (v_chr_filter, v_chr_filter_pos, v_chr_filter_size) = init_filter(
            chr_yinc,
            chr_src_h,
            chr_dst_h,
            FILTER_ALIGN,
            1 << 12,
            get_local_pos(chr_src_vsub_sample.into(), -513),
            get_local_pos(chr_src_vsub_sample.into(), -513),
        )?;

        let mut src_bpc = pix_fmt.comps()[0].depth();
        if src_bpc < 8 {
            src_bpc = 8;
        }

        let mut lum_buf_size: usize = 0;
        let mut chr_buf_size: usize = 0;

        get_min_buffer_size(
            &v_lum_filter_pos,
            v_lum_filter_size,
            &v_chr_filter_pos,
            v_chr_filter_size,
            dst_height,
            isize::try_from(chr_dst_h).expect("hope dst_h fit isize"),
            chr_src_vsub_sample,
            &mut lum_buf_size,
            &mut chr_buf_size,
        );

        let mut dst_stride = ffalign!(dst_width.get() * 2 + 66, 16);
        //int dst_stride = FFALIGN(c->dstW * sizeof(int16_t) + 66, 16);
        //
        if src_bpc == 16 {
            dst_stride <<= 1;
        }
        if src_bpc == 32 {
            dst_stride <<= 2;
        }

        let num_ydesc = 1;
        let num_cdesc = 1;

        let num_vdesc = if pix_fmt.is_planar_yuv() { 2 } else { 1 };
        let num_desc = num_ydesc + num_cdesc + num_vdesc;
        let desc_index = [num_ydesc, num_ydesc + num_cdesc];
        //c->desc = av_calloc(c->numDesc, sizeof(*c->desc));

        let slice0 = alloc_slice(
            src_height.get(),
            chr_src_h,
            chr_src_hsub_sample,
            chr_src_vsub_sample,
            false,
        );

        /*for (i = 1; i < c->numSlice - 2; ++i) {
          res = alloc_slice(&c->slice[i], c->srcFormat, lumBufSize, chrBufSize,
                            c->chrSrcHSubSample, c->chrSrcVSubSample, 0);
          if (res < 0)
            goto cleanup;
          res = alloc_lines(&c->slice[i], FFALIGN(c->srcW * 2 + 78, 16), c->srcW);
          if (res < 0)
            goto cleanup;
        }*/

        lum_buf_size = lum_buf_size.max(v_lum_filter_size + usize::from(MAX_LINES_AHEAD));
        chr_buf_size = chr_buf_size.max(v_chr_filter_size + usize::from(MAX_LINES_AHEAD));

        // Horizontal scaler output.
        let slice1 = alloc_slice(
            u16::try_from(lum_buf_size).expect("hope buf_size fit u16"),
            u16::try_from(chr_buf_size).expect("hope buf_size fit u16"),
            chr_src_hsub_sample,
            chr_src_vsub_sample,
            true,
        );

        // Vertical scaler output.
        let slice2 = alloc_slice(
            dst_height.get(),
            chr_dst_h,
            chr_src_hsub_sample,
            chr_src_vsub_sample,
            false,
        );

        let mut slices = Slices {
            slice0,
            slice1,
            slice2,
        };

        alloc_lines(&mut slices, usize::from(dst_stride), dst_width);

        /*fill_ones(
            c.slice[1].as_mut().unwrap(),
            isize::try_from(dst_stride >> 1).unwrap(),
            c.src_bpc.into(),
        );*/

        let desc0 = init_desc_hscale(
            h_lum_filter,
            h_lum_filter_pos,
            h_lum_filter_size,
            /*c.lum_xinc,*/
        );

        let desc1 = init_desc_chscale(
            h_chr_filter,
            h_chr_filter_pos,
            h_chr_filter_size,
            /*c.chr_xinc,*/
        );

        let filters = Filters {
            v_lum_filter,
            v_lum_filter_pos,
            v_lum_filter_size,
            v_chr_filter,
            v_chr_filter_pos,
            v_chr_filter_size,
        };

        let (yuv2plane_x, hy_scale, hc_scale) = init_scale(src_bpc);

        let (desc2, desc3) = init_vscale(&filters, yuv2plane_x);
        //res = ff_init_vscale(c, c->desc + index, c->slice + srcIdx, c->slice + dstIdx);

        Ok(Self {
            src_width,
            src_height,
            dst_width,
            dst_height,

            chr_src_vsub_sample,
            desc_index,
            num_desc,

            //yuv2plane_x,
            hy_scale,
            hc_scale,

            pix_fmt,
            slices,
            desc: [desc0, desc1, desc2, desc3],
            filters,

            chr_src_h,
        })
    }

    /// Scales `src_frame` to another size and stores it in `dst_frame`.
    ///
    /// Will check that `src_frame` matches scaler configuration. `dst_frame` will be resized if necessary.
    pub fn scale(&mut self, src_frame: &Frame, dst_frame: &mut Frame) -> Result<(), ScalerError> {
        use ScalerError::*;

        let src_width = self.src_width;
        let src_height = self.src_height;
        let pix_fmt = self.pix_fmt;
        let dst_width = self.dst_width;
        let dst_height = self.dst_height;

        if src_frame.width() != src_width {
            return Err(WidthChanged(src_frame.width(), src_width));
        }
        if src_frame.height() != src_height {
            return Err(HeightChanged(src_frame.height(), src_height));
        }
        if src_frame.pix_fmt() != pix_fmt {
            return Err(PixelFormatChanged(src_frame.pix_fmt(), pix_fmt));
        }

        dst_frame.reset_buffer(dst_width, dst_height, pix_fmt, 1)?;
        dst_frame.set_width(dst_width);
        dst_frame.set_height(dst_height);
        dst_frame.set_pix_fmt(pix_fmt);
        dst_frame.set_color_range(src_frame.color_range());

        let scaler = Mutex::new(self);
        let dst_frame = Mutex::new(dst_frame);

        let result = panic::catch_unwind(|| {
            let mut dst_frame = dst_frame.lock().expect("not poisoned");
            let (dst_linesize, dst_data) = dst_frame.line_and_data_mut();
            scaler.lock().expect("not poisoned").scale_internal(
                src_frame.data(),
                src_frame.linesize(),
                dst_data,
                dst_linesize,
            );
        });
        if result.is_err() {
            return Err(ScalerError::Panic(format!(
                "src_width={src_width} src_height={src_height} pix_fmt={pix_fmt} dst_width={dst_width} dst_height={dst_height}",
            )));
        }
        Ok(())
    }

    #[allow(
        clippy::too_many_arguments,
        clippy::similar_names,
        clippy::too_many_lines
    )]
    fn scale_internal(
        &mut self,
        src: &[Vec<u8>; 8],
        src_stride: &[usize; 8],
        dst: &mut [Vec<u8>; 8],
        dst_stride: &[usize; 8],
    ) {
        if self.pix_fmt.is_packed() {
            todo!();
            //src[1] = src[0].to_owned();
            //src[2] = src[0].to_owned();
            //src[3] = src[0].to_owned();
            //src_stride[1] = src_stride[0];
            //src_stride[2] = src_stride[0];
            //src_stride[3] = src_stride[0];
        }

        /*let (chr_ctx, lum_ctx) = init_vscale_pfn(&self.filters, self.yuv2plane_x);
         *self.desc[3].instance.v_scaler_mut() = chr_ctx;
         *self.desc[2].instance.v_scaler_mut() = lum_ctx;*/

        let chr_src_slice_h = ceil_rshift(self.src_height.get(), self.chr_src_vsub_sample);
        init_slice_from_src(
            &mut self.slices.slice0,
            true,
            src_stride,
            self.src_width,
            self.src_height.get(),
            chr_src_slice_h,
        );

        init_slice_from_src(
            &mut self.slices.slice2,
            false,
            dst_stride,
            self.dst_width,
            self.dst_height.get(),
            ceil_rshift(self.dst_height.get(), self.chr_src_vsub_sample),
        );

        let hout_slice = &mut self.slices.slice1;
        for i in 0..4 {
            hout_slice.plane[i].slice_y = 0;
            hout_slice.plane[i].slice_h = 0;
        }
        hout_slice.width = self.dst_width;

        // False positive?
        #[allow(clippy::eq_op)]
        let scale_dst = self.dst_height < self.dst_height;
        let dst_h = if scale_dst {
            self.dst_height.get() + self.dst_height.get()
        } else {
            self.dst_height.get()
        };

        let lum_end = self.desc_index[0];
        let chr_start = lum_end;
        let chr_end = self.desc_index[1];
        let v_start = chr_end;
        let v_end = self.num_desc;

        for dst_y in 0..dst_h {
            //for (; dstY < dstH; dstY++) {
            let chr_src_y = dst_y >> self.chr_src_vsub_sample;

            // First line needed as input
            let a = 1 - isize::try_from(self.filters.v_lum_filter_size)
                .expect("hope filter_size fit isize");
            let b = isize::try_from(self.filters.v_lum_filter_pos[usize::from(dst_y)])
                .expect("hope filter_pos fit isize");
            let first_lum_src_y = isize::max(a, b);

            // First line needed as input
            let first_chr_src_y = isize::max(
                1 - isize::try_from(self.filters.v_chr_filter_size)
                    .expect("hope filter_size fit isize"),
                isize::try_from(self.filters.v_chr_filter_pos[usize::from(chr_src_y)])
                    .expect("hope filter_pos fit isize"),
            );

            // Last line needed as input
            let last_lum_src_y = isize::min(
                isize::try_from(self.src_height.get()).expect("hope src_height fit isize"),
                first_lum_src_y
                    + isize::try_from(self.filters.v_lum_filter_size)
                        .expect("hope filter_size fit isize"),
            ) - 1;
            let last_chr_src_y = isize::min(
                isize::try_from(self.chr_src_h).expect("hope src_h fit isize"),
                first_chr_src_y
                    + isize::try_from(self.filters.v_chr_filter_size)
                        .expect("hope filter_size fit isize"),
            ) - 1;

            let hout_slice = &mut self.slices.slice1;
            assert!(
                (last_lum_src_y - first_lum_src_y + 1)
                    <= isize::try_from(hout_slice.plane[0].available_lines)
                        .expect("hope available_lines fit isize")
            );
            assert!(
                (last_chr_src_y - first_chr_src_y + 1)
                    <= isize::try_from(hout_slice.plane[1].available_lines)
                        .expect("hope available_lines fit isize")
            );

            let pos_y = hout_slice.plane[0].slice_y + hout_slice.plane[0].slice_h;
            let first_pos_y = pos_y;
            let last_pos_y = last_lum_src_y;

            let c_pos_y = hout_slice.plane[1].slice_y + hout_slice.plane[1].slice_h;
            let first_cpos_y = c_pos_y;
            let last_cpos_y = last_chr_src_y;

            rotate_slice(hout_slice, last_pos_y, last_cpos_y);

            if pos_y < last_lum_src_y + 1 {
                for i in 0..usize::from(lum_end) {
                    let process = self.desc[i].process;
                    (process)(self, i, first_pos_y, last_pos_y - first_pos_y + 1, src, dst);
                }
            }

            if c_pos_y < last_chr_src_y + 1 {
                for i in chr_start..chr_end {
                    let i = usize::from(i);
                    let process = self.desc[i].process;
                    (process)(
                        self,
                        i,
                        first_cpos_y,
                        last_cpos_y - first_cpos_y + 1,
                        src,
                        dst,
                    );
                }
            }

            /*if dst_y >= self.dst_height - 2 {
                // hmm looks like we can't use MMX here without overwriting this array's tail.
                self.yuv2plane_x = init_output_funcs();
                let (chr_ctx, lum_ctx) = init_vscale_pfn(&self.filters, self.yuv2plane_x);
                *self.desc[3].instance.v_scaler_mut() = chr_ctx;
                *self.desc[2].instance.v_scaler_mut() = lum_ctx;
            }*/

            for i in v_start..v_end {
                let i = usize::from(i);
                let process = self.desc[i].process;
                (process)(
                    self,
                    i,
                    isize::try_from(dst_y).expect("hope dst_y fit isize"),
                    1,
                    src,
                    dst,
                );
            }
        }

        // Store changed local vars back in the context.
        //c->dstY = dstY;

        //return dstY - lastDstY;
    }

    fn get_src_slice(&self, desc_i: usize) -> &SwsSlice {
        let slice_index = Self::get_src_index(desc_i);
        self.slices.get_slice(slice_index)
    }

    fn get_dst_slice(&mut self, desc_i: usize) -> &SwsSlice {
        let slice_index = Self::get_dst_index(desc_i);
        self.slices.get_slice(slice_index)
    }

    fn get_dst_slice_mut(&mut self, desc_i: usize) -> &mut SwsSlice {
        let slice_index = Self::get_dst_index(desc_i);
        self.slices.get_slice_mut(slice_index)
    }

    fn get_src_index(desc_index: usize) -> SliceI {
        match desc_index {
            0 | 1 => SliceI::I0,
            2 | 3 => SliceI::I1,
            _ => panic!("get_src_index"),
        }
    }

    fn get_dst_index(desc_index: usize) -> SliceI {
        match desc_index {
            0 | 1 => SliceI::I1,
            2 | 3 => SliceI::I2,
            _ => panic!("get_dst_index"),
        }
    }
}

fn get_local_pos(chr_subsample: isize, mut pos: isize) -> isize {
    if pos == -1 || pos <= -513 {
        pos = (128 << chr_subsample) - 128;
    }
    pos += 128; // Relative to ideal left edge.
    pos >> chr_subsample
}

#[allow(clippy::struct_field_names)]
struct Filters {
    // Array of vertical filter coefficients for luma/alpha planes.
    v_lum_filter: Rc<Vec<i16>>,
    // Array of vertical filter starting positions for each dst[i] for luma/alpha planes.
    v_lum_filter_pos: Rc<Vec<i32>>,
    //int hChrFilterSize;
    v_lum_filter_size: usize,

    // Array of vertical filter coefficients for chroma planes.
    v_chr_filter: Rc<Vec<i16>>,
    // Array of vertical filter starting positions for each dst[i] for chroma planes.
    v_chr_filter_pos: Rc<Vec<i32>>,
    // Vertical filter size for chroma pixels.
    v_chr_filter_size: usize,
}
