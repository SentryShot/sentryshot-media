// SPDX-License-Identifier: MPL-2.0+

use crate::{scale::Yuv2planarXFn, CreateScalerError, Scaler};
use std::rc::Rc;

//#define ROUNDED_DIV(a,b) (((a)>=0 ? (a) + ((b)>>1) : (a) - ((b)>>1))/(b))
macro_rules! rounded_div {
    ($a:expr, $b:expr) => {
        (if ($a) >= 0 {
            ($a) + (($b) >> 1)
        } else {
            ($a) - (($b) >> 1)
        }) / $b
    };
}

#[allow(
    clippy::type_complexity,
    clippy::items_after_statements,
    clippy::too_many_lines,
    clippy::similar_names
)]
pub(crate) fn init_filter(
    x_inc: isize,
    src_w: u16,
    dst_w: u16,
    filter_align: i32,
    one: isize,
    src_pos: isize,
    dst_pos: isize,
) -> Result<(Rc<Vec<i16>>, Rc<Vec<i32>>, usize), CreateScalerError> {
    let src_w2 = usize::from(src_w);
    let src_w3 = i32::from(src_w);
    let dst_w2 = usize::from(dst_w);
    let dst_w3 = i32::from(dst_w);

    const SIZE_FACTOR: i32 = 4;
    let filter_size: i32 = if x_inc <= 1 << 16 {
        1 + SIZE_FACTOR // Upscale.
    } else {
        1 + (SIZE_FACTOR * src_w3 + dst_w3 - 1) / dst_w3
    }
    .min(src_w3 - 2)
    .max(1);

    let filter_size2 = usize::try_from(filter_size).expect("hope filter_size fit usize");

    let mut filter: Vec<i64> = vec![0; dst_w2 * filter_size2];
    //FF_ALLOC_TYPED_ARRAY(filter, dstW * filterSize)

    let dst_pos = i64::try_from(dst_pos).expect("hope dst_pos fit i64");
    let x_inc2 = i64::try_from(x_inc).expect("hope x_inc fit i64");
    let src_pos = i64::try_from(src_pos).expect("hope src_pos fit i64");
    let mut x_dst_in_src: i64 = ((dst_pos * x_inc2) >> 7) - ((src_pos * 0x10000_i64) >> 7);

    // NOTE: the +3 is for the MMX(+1) / SSE(+3) scaler which reads over the end
    let mut filter_pos: Vec<i32> = vec![0; dst_w2 + 3];
    //FF_ALLOC_TYPED_ARRAY(*filterPos, dstW + 3)

    let fone = 1
        << (54
            - i64::try_from(log2_c(src_w2 / dst_w2))
                .expect("hope fone fit i64")
                .min(8));

    for i in 0..dst_w2 {
        let x_dst_in_src2 = i32::try_from(x_dst_in_src).expect("hope x_dst_in_src fit i32");
        let mut xx = (x_dst_in_src2 - (filter_size - 2) * (1 << 16)) / (1 << 17);

        filter_pos[i] = xx;

        for j in 0..filter_size2 {
            let mut d = i64::abs((i64::from(xx) * (1 << 17)) - x_dst_in_src) << 13;
            //int64_t d = (FFABS(((int64_t)xx * (1 << 17)) - xDstInSrc)) << 13;

            let mut coeff: i64;

            if x_inc > 1 << 16 {
                d = d * i64::from(dst_w) / i64::from(src_w);
            }

            #[allow(
                clippy::cast_precision_loss,
                clippy::cast_possible_truncation,
                clippy::as_conversions
            )]
            let c: i64 = (0.6 * f64::from(1 << 24)) as i64;
            //int64_t C = 0.6 * (1 << 24);

            if d >= 1_i64 << 31 {
                coeff = 0;
            } else {
                let dd: i64 = (d * d) >> 30;
                let ddd = (dd * d) >> 30;

                if d < 1_i64 << 30 {
                    coeff = (12 * (1 << 24) - 6 * c) * ddd
                        + (-18 * (1 << 24) + 6 * c) * dd
                        + 6 * (1 << 24) * (1 << 30);
                } else {
                    coeff = (-0 - 6 * c) * ddd
                        + (30 * c) * dd
                        + (0 - 48 * c) * d
                        + (24 * c) * (1 << 30);
                }
            }

            coeff /= (1_i64 << 54) / fone;

            filter[i * filter_size2 + j] = coeff;

            xx += 1;
        }

        x_dst_in_src += 2 * i64::try_from(x_inc).expect("hope x_inc fit i64");
    }

    // apply src & dst Filter to filter -> filter2
    assert!(filter_size > 0);
    let filter2_size: i32 = filter_size;
    let filter2_size2 = usize::try_from(filter2_size).expect("hope filter2_size fit usize");

    let mut filter2 = vec![0; (dst_w2 * filter2_size2) + 1];
    //FF_ALLOCZ_TYPED_ARRAY(filter2, dstW * filter2Size)

    for i in 0..dst_w2 {
        for j in 0..filter_size2 {
            filter2[i * filter2_size2 + j] = filter[i * filter_size2 + j];
        }
        filter_pos[i] += (filter_size - 1) / 2 - (filter2_size - 1) / 2;
    }

    // Try to reduce the filter-size (step1 find size and shift left).
    // Assume it is near normalized (*0.5 or *2.0 is OK but * 0.001 is not).
    let mut min_filter_size: i32 = 0;
    for i in (0..dst_w).rev() {
        let i2 = usize::from(i);
        //for (i = dstW - 1; i >= 0; i--) {
        let mut min: i32 = filter2_size;
        let mut cut_off: i64 = 0;
        //int64_t cutOff = 0.0;

        // Get rid of near zero elements on the left by shifting left.
        for _ in 0..filter2_size2 {
            cut_off += (filter2[i2 * filter2_size2]).abs();
            //cutOff += FFABS(filter2[i * filter2Size]);
            #[allow(clippy::cast_precision_loss, clippy::as_conversions)]
            if cut_off as f64 > MAX_REDUCE_CUTOFF * fone as f64 {
                break;
            }

            // preserve monotonicity because the core can't handle the filter otherwise.
            if i < dst_w - 1 && filter_pos[i2] >= filter_pos[i2 + 1] {
                break;
            }

            // move filter coefficients left
            let mut k: usize = 1;
            while k < filter2_size2 {
                //for (k = 1; k < filter2Size; k++)
                filter2[i2 * filter2_size2 + k - 1] = filter2[i2 * filter2_size2 + k];
                k += 1;
            }
            filter2[i2 * filter2_size2 + k - 1] = 0;
            filter_pos[i2] += 1;
        }

        // Count near zeros on the right.
        let mut cut_off: i64 = 0;
        for j in (0..=filter2_size2).rev() {
            //for (j = filter2Size - 1; j > 0; j--) {
            cut_off += filter2[usize::from(i) * filter2_size2 + j].abs();
            #[allow(clippy::cast_precision_loss, clippy::as_conversions)]
            if cut_off as f64 > MAX_REDUCE_CUTOFF * fone as f64 {
                //if (cutOff > SWS_MAX_REDUCE_CUTOFF * fone)
                break;
            }
            min -= 1;
        }

        if min > min_filter_size {
            min_filter_size = min;
        }
    }

    /*if (PPC_ALTIVEC(cpu_flags)) {
      // we can handle the special case 4, so we don't want to go the full 8
      if (minFilterSize < 5)
        filterAlign = 4;

      /* We really don't want to waste our time doing useless computation, so
       * fall back on the scalar C code for very small filters.
       * Vectorizing is worth it only if you have a decent-sized vector. */
      if (minFilterSize < 3)
        filterAlign = 1;
    }*/

    /*if (have_lasx(cpu_flags)) {
      int reNum = minFilterSize & (0x07);

      if (minFilterSize < 5)
        filterAlign = 4;
      if (reNum < 3)
        filterAlign = 1;
    }*/

    assert!(min_filter_size > 0);
    let filter_size = (min_filter_size + (filter_align - 1)) & (!(filter_align - 1));
    let filter_size2 = usize::try_from(filter_size).expect("hope filter_size fit usize");
    //filterSize = (minFilterSize + (filterAlign - 1)) & (~(filterAlign - 1));
    assert!(filter_size > 0);
    filter = vec![0; dst_w2 * filter_size2];
    //filter = av_malloc_array(dstW, filterSize * sizeof(*filter));
    //
    if filter_size >= i32::from(MAX_FILTER_SIZE) * 16 / (16) {
        return Err(CreateScalerError::CascadedUnsupported);
    }
    /*if (filterSize >= MAX_FILTER_SIZE * 16 / (16)) {
      ret = RETCODE_USE_CASCADE;
      goto fail;
    }*/

    // Try to reduce the filter-size (step2 reduce it).
    for i in 0..dst_w2 {
        for j in 0..filter_size2 {
            if j >= filter2_size2 {
                filter[i * filter_size2 + j] = 0;
            } else {
                filter[i * filter_size2 + j] = filter2[i * filter2_size2 + j];
            }
            let j2 = i32::try_from(j).expect("hope j fit i32");
            if j2 >= min_filter_size {
                filter[i * filter_size2 + j] = 0;
            }
        }
    }

    // Fix borders.
    for i in 0..dst_w2 {
        if (filter_pos[i]).is_negative() {
            // Move filter coefficients left to compensate for filterPos.
            for j in 1..filter_size {
                let left =
                    usize::try_from((j + filter_pos[i]).max(0)).expect("hope left fit usize");
                let j = usize::try_from(j).expect("hope j fit usize");
                filter[i * filter_size2 + left] += filter[i * filter_size2 + j];
                filter[i * filter_size2 + j] = 0;
            }
            filter_pos[i] = 0;
        }

        if filter_pos[i] + filter_size > src_w3 {
            let shift = usize::try_from(filter_pos[i] + (filter_size - src_w3).min(0))
                .expect("hope shift fit usize");
            let mut acc: i64 = 0;

            for j in (0..filter_size2).rev() {
                let pos = isize::try_from(filter_pos[i]).expect("hope filter_pos fit isize");
                let j2 = isize::try_from(j).expect("hope j fit isize");
                let src_w = isize::try_from(src_w).expect("hope src_w fit isize");
                if pos + j2 >= src_w {
                    acc += filter[i * filter_size2 + j];
                    filter[i * filter_size2 + j] = 0;
                }
            }

            for j in (0..filter_size2).rev() {
                if j < shift {
                    filter[i * filter_size2 + j] = 0;
                } else {
                    filter[i * filter_size2 + j] = filter[i * filter_size2 + j - shift];
                }
            }

            filter_pos[i] -= i32::try_from(shift).expect("hope shift fit i32");
            let pos = usize::try_from(filter_pos[i]).expect("hope filter_pos fit usize");
            filter[i * filter_size2 + usize::from(src_w) - 1 - pos] += acc;
            //filter[i * filterSize + srcW - 1 - (*filterPos)[i]] += acc;
        }
        assert!(filter_pos[i] >= 0);
        assert!((filter_pos[i]) < src_w3);
        if filter_pos[i] + filter_size > src_w3 {
            //if ((*filterPos)[i] + filterSize > srcW) {
            for j in 0..filter_size {
                let j2 = usize::try_from(j).expect("hope j fit usize");
                //for (j = 0; j < filterSize; j++) {
                assert!(((filter_pos[i] + j) < src_w3) || filter[i * filter_size2 + j2] != 0);
                //av_assert0((*filterPos)[i] + j < srcW || !filter[i * filterSize + j]);
            }
        }
    }

    let out_filter_size = filter_size2;

    // Note the +1 is for the MMX scaler which reads over the end.
    // align at 16 for AltiVec (needed by hScale_altivec_real).
    let mut out_filter: Vec<i16> = vec![0; out_filter_size * (dst_w2 + 3)];
    //FF_ALLOCZ_TYPED_ARRAY(*outFilter, *outFilterSize * (dstW + 3))

    // Normalize & store in outFilter.
    for i in 0..dst_w2 {
        let mut error: i64 = 0;
        let mut sum: i64 = 0;

        for j in 0..filter_size2 {
            sum += filter[i * filter_size2 + j];
        }

        let one = i64::try_from(one).expect("hope one fit i64");
        sum = (sum + one / 2) / one;
        //  av_log(NULL, AV_LOG_WARNING, "SwScaler: zero vector in scaling\n");
        assert!(sum != 0, "zero vector in scaling");
        for j in 0..out_filter_size {
            let v: i64 = filter[i * filter_size2 + j] + error;
            let int_v: i16 = i16::try_from(rounded_div!(v, sum)).expect("hope int_v fit i16");

            out_filter[i * out_filter_size + j] = int_v;
            error = v - i64::from(int_v) * sum;
        }
    }

    filter_pos[dst_w2] = filter_pos[dst_w2 - 1];
    filter_pos[dst_w2 + 1] = filter_pos[dst_w2 - 1];
    filter_pos[dst_w2 + 2] = filter_pos[dst_w2 - 1];

    // The MMX/SSE scaler will read over the end.
    for i in 0..out_filter_size {
        let k = (dst_w2 - 1) * out_filter_size + i;
        out_filter[k + out_filter_size] = out_filter[k];
        out_filter[k + 2 * out_filter_size] = out_filter[k];
        out_filter[k + 3 * out_filter_size] = out_filter[k];
    }
    /*for (i = 0; i < *outFilterSize; i++) {
      int k = (dstW - 1) * (*outFilterSize) + i;
      (*outFilter)[k + 1 * (*outFilterSize)] =
          (*outFilter)[k + 2 * (*outFilterSize)] =
              (*outFilter)[k + 3 * (*outFilterSize)] = (*outFilter)[k];
    }*/

    Ok((Rc::new(out_filter), Rc::new(filter_pos), out_filter_size))
}

// Struct which holds all necessary data for processing a slice.
// A processing step can be a color conversion or horizontal/vertical scaling.
pub(crate) struct SwsFilterDescriptor {
    pub instance: Instance,
    //void *instance; ///< Filter instance data

    // Function for processing input slice sliceH lines starting from line sliceY.
    // WRITE: init_desc_hscale, init_desc_chscale, init_vscale
    pub process: ProcessFn,
    //int (*process)(SwsContext *c, struct SwsFilterDescriptor *desc, int sliceY, int sliceH);
}

type ProcessFn = fn(
    c: &mut Scaler,
    desc_index: usize,
    sliceY: isize,
    sliceH: isize,
    src2: &[Vec<u8>],
    dst2: &mut [Vec<u8>],
);

pub(crate) enum Instance {
    VScalerContext(VScalerContext),
    FilterContext(FilterContext),
}

impl Instance {
    pub fn v_scaler(&self) -> &VScalerContext {
        match self {
            Instance::VScalerContext(v) => v,
            Instance::FilterContext(_) => todo!(),
        }
    }
    /*pub fn v_scaler_mut(&mut self) -> &mut VScalerContext {
        match self {
            Instance::VScalerContext(v) => v,
            Instance::FilterContext(_) => todo!(),
        }
    }*/
    pub fn filter(&self) -> &FilterContext {
        match self {
            Instance::VScalerContext(_) => todo!(),
            Instance::FilterContext(v) => v,
        }
    }
}

pub(crate) struct VScalerContext {
    pub filter: [Rc<Vec<i16>>; 2],
    //uint16_t *filter[2];
    pub filter_pos: Rc<Vec<i32>>,
    //int32_t *filter_pos;
    pub filter_size: usize,
    //int filter_size;
    pub yuv2planar_x: Option<Yuv2planarXFn>,
    //yuv2planarX_fn yuv2planarX;
}

/// Scaler instance data
pub(crate) struct FilterContext {
    pub filter: Rc<Vec<i16>>,
    pub filter_pos: Rc<Vec<i32>>,
    pub filter_size: usize,
    //x_inc: isize,
}

const MAX_REDUCE_CUTOFF: f64 = 0.002;
const MAX_FILTER_SIZE: u16 = 256;

fn log2_c(mut v: usize) -> isize {
    //static av_always_inline av_const int ff_log2_c(unsigned int v)

    let mut n: isize = 0;
    //int n = 0;
    if v & 0xffff_0000 != 0 {
        v >>= 16;
        n += 16;
    }
    /*if (v & 0xffff0000) {
        v >>= 16;
        n += 16;
    }*/
    if v & 0xff00 != 0 {
        v >>= 8;
        n += 8;
    }
    /*if (v & 0xff00) {
        v >>= 8;
        n += 8;
    }*/

    n += isize::from(LOG2_TAB[v]);
    //n += ff_log2_tab[v];

    n
}

const LOG2_TAB: [u8; 256] = [
    0, 0, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
    5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5,
    6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
    6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
];

#[allow(unused)]
pub(crate) fn shuffle_filter_coefficients(
    //scaler: &mut Scaler,
    _filter_pos: &mut [i32],
    _filter_size: usize,
    filter: &mut Vec<i16>,
    _dst_w: u16,
) {
    //int ff_shuffle_filter_coefficients(SwsContext *c, int *filterPos,
    //                                   int filterSize, int16_t *filter, int dstW) {
    //#if ARCH_X86_64
    {
        //int i, j, k;
        //int cpu_flags = av_get_cpu_flags();
        if filter.is_empty() {
            //return;
        }
        //if (!filter)
        //  return 0;
        /*if (EXTERNAL_AVX2_FAST(cpu_flags) && !(cpu_flags & AV_CPU_FLAG_SLOW_GATHER)) {
          if ((c->srcBpc == 8) && (c->srcBpc <= 14)) {
            int16_t *filterCopy = NULL;
            if (filterSize > 4) {
              if (!FF_ALLOC_TYPED_ARRAY(filterCopy, dstW * filterSize))
                return AVERROR(ENOMEM);
              memcpy(filterCopy, filter, dstW * filterSize * sizeof(int16_t));
            }
            // Do not swap filterPos for pixels which won't be processed by
            // the main loop.
            for (i = 0; i + 16 <= dstW; i += 16) {
              FFSWAP(int, filterPos[i + 2], filterPos[i + 4]);
              FFSWAP(int, filterPos[i + 3], filterPos[i + 5]);
              FFSWAP(int, filterPos[i + 10], filterPos[i + 12]);
              FFSWAP(int, filterPos[i + 11], filterPos[i + 13]);
            }
            if (filterSize > 4) {
              // 16 pixels are processed at a time.
              for (i = 0; i + 16 <= dstW; i += 16) {
                // 4 filter coeffs are processed at a time.
                for (k = 0; k + 4 <= filterSize; k += 4) {
                  for (j = 0; j < 16; ++j) {
                    int from = (i + j) * filterSize + k;
                    int to = i * filterSize + j * 4 + k * 16;
                    memcpy(&filter[to], &filterCopy[from], 4 * sizeof(int16_t));
                  }
                }
              }
              // 4 pixels are processed at a time in the tail.
              for (; i < dstW; i += 4) {
                // 4 filter coeffs are processed at a time.
                int rem = dstW - i >= 4 ? 4 : dstW - i;
                for (k = 0; k + 4 <= filterSize; k += 4) {
                  for (j = 0; j < rem; ++j) {
                    int from = (i + j) * filterSize + k;
                    int to = i * filterSize + j * 4 + k * 4;
                    memcpy(&filter[to], &filterCopy[from], 4 * sizeof(int16_t));
                  }
                }
              }
            }
            av_free(filterCopy);
          }
        }*/
    }
}

/*mod tests {
    use super::*;

    #[test]
    fn test_log() {
        for i in 0..usize::MAX {
            let a = log2_c(i);
            let b = i.ilog2();
        }
    }
}*/
